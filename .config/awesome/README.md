# AwesomeWM Configuration

Really AwesomeWM Configuration.

## Folder Structure

folders which are not listed is code from other developers

- `core` manage things that are necessary for it to be called a desktop
  environment such as notification, bar, hotkeys, etc.
- `lib` homemade library
- `scripts` shell scripts for doing things
- `themes` configuration to make the looks consistent
- `ui` widgets to increase user experience

## Optional Dependencies

Some functionality depend on these programs

- `picom`
- `playerctl`
- `Cudiph/pulseaudio-dbus`

## Custom Signals

signals that are emitted in this configuration

- `volume::change`
- `volume::toggle`
- `brightness::change`
