local gears        = require("gears")
local naughty      = require("naughty")
local lain         = require("lain")
local awful        = require("awful")
local wibox        = require("wibox")
local theme        = require("beautiful")
local markup       = lain.util.markup
local my_table     = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi          = require("beautiful.xresources").apply_dpi
local player_ctrl  = require("ui.player")
local notif_widget = require("ui.notif_history")
local pulseaudio   = require("ui.pulseaudio")
local lib          = require("lib")

local mouse        = mouse
local screen       = screen
local client       = client

local function get_client_index(c, tags)
    local counter = 0
    for i, tag in pairs(tags) do
        for j, val in ipairs(tag:clients()) do
            if c == val then return counter end
            counter = counter + 1
        end
    end
end

local notif_man = lib.managers.naughty_mgr

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local mytextclock = wibox.widget {
    format = markup(theme.colorscheme[1], "󰥔 %a %d %b ")
        .. markup("#fff", ">")
        .. markup(theme.colorscheme[0], " %H:%M:%S "),
    refresh = 1,
    font = theme.wibar_font,
    widget = wibox.widget.textclock,
}

-- Calendar
local _ = lain.widget.cal {
    attach_to = { mytextclock },
    notification_preset = {
        font = theme.wibar_font,
        fg = theme.fg_normal,
        bg = theme.bg_normal,
    },
}

-- Battery
local bat = lain.widget.bat {
    settings = function ()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc
        perc = "󰁹 " .. perc

        if bat_now.ac_status == 1 then perc = perc .. " AC" end

        widget:set_markup(markup.fontfg(theme.wibar_font, theme.colorscheme[2], perc .. " "))
    end,
}

-- Weather
-- to be set before use
local weather = lain.widget.weather {
    APPID = os.getenv("OPENWEATHER_APIKEY"),
    lang = "id",
    city_id = os.getenv("OPENWEATHER_CITY_ID"),
    notification_preset = { font = "JetBrainsMono Nerd Font Bold 10", fg = theme.fg_normal },
    weather_na_markup = markup.fontfg(theme.wibar_font, theme.colorscheme[3], "N/A "),
    settings = function ()
        descr = weather_now["weather"][1]["main"]
        units = math.floor(weather_now["main"]["temp"])
        widget:set_markup(
            markup.fontfg(theme.wibar_font, theme.colorscheme[3], "󰢘 " .. descr .. " " .. units .. "°C ")
        )
    end,
}
--]]

-- / fs
-- commented because it needs Gio/Glib >= 2.54
local fs = lain.widget.fs {
    notification_preset = { font = "JetBrainsMono Nerd Font 10", fg = theme.fg_normal },
    settings = function ()
        widget:set_markup(
            markup.fontfg(
                theme.wibar_font,
                theme.colorscheme[4],
                "󰋊 " .. string.format("%.1f", fs_now["/"].used) .. "% "
            )
        )
    end,
}
--]]

-- MEM
local memory = lain.widget.mem {
    settings = function ()
        widget:set_markup(markup.fontfg(theme.wibar_font, theme.colorscheme[5], "󰍛 " .. mem_now.used .. "M "))
    end,
}

-- CPU
local cpu = lain.widget.cpu {
    settings = function ()
        widget:set_markup(markup.fontfg(theme.wibar_font, theme.colorscheme[6], "󰻠 " .. cpu_now.usage .. "% "))
    end,
}

-- Coretemp
local temp = lain.widget.temp {
    settings = function ()
        widget:set_markup(markup.fontfg(theme.wibar_font, theme.colorscheme[7], " " .. coretemp_now .. "°C "))
    end,
}

-- Pulseaudio volume
local pulseaudio_wdgt = pulseaudio.new()
local player_popup = awful.popup {
    widget = player_ctrl,
    width = 500,
    height = 500,
    ontop = true,
    shape = gears.shape.rounded_rect,
    border_color = "#ffffff",
    border_width = 1,
    visible = false,
    -- placement = function(obj, args)
    --     local top = (awful.placement.no_offscreen + awful.placement.top)
    --     top(obj, { margins = { top = dpi(30) } })
    -- end,
}
player_popup:connect_signal("button::press", function (_, _, _, b)
    if b == awful.button.names.RIGHT then player_popup.visible = false end
end)
pulseaudio_wdgt:connect_signal("button::press", function (_, _, _, btn)
    if btn == awful.button.names.LEFT then
        player_popup:move_next_to(mouse.current_widget_geometry)
    elseif btn == awful.button.names.RIGHT then
        player_popup.visible = false
    end
end)
pulseaudio_wdgt:get_children_by_id("sink_textbox")[1]:connect_signal("button::press", function (_, _, _, btn)
    if btn == awful.button.names.SCROLL_UP then
        awesome.emit_signal("volume::change", "+3")
    elseif btn == awful.button.names.SCROLL_DOWN then
        awesome.emit_signal("volume::change", "-3")
    elseif btn == awful.button.names.MIDDLE then
        awful.spawn.with_shell("pactl set-sink-mute @DEFAULT_SINK@ toggle")
    end
end)
pulseaudio_wdgt:get_children_by_id("source_textbox")[1]:connect_signal("button::press", function (_, _, _, btn)
    if btn == awful.button.names.SCROLL_UP then
        awful.spawn.with_shell("pactl set-source-volume @DEFAULT_SOURCE@ +3%")
    elseif btn == awful.button.names.SCROLL_DOWN then
        awful.spawn.with_shell("pactl set-source-volume @DEFAULT_SOURCE@ -3%")
    elseif btn == awful.button.names.MIDDLE then
        awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
    end
end)
pulseaudio_wdgt:connect_signal("mouse::enter", function (self)
    self.prev_fg = self.fg
    self.fg = "#fff"
end)
pulseaudio_wdgt:connect_signal("mouse::leave", function (self)
    self.fg = self.prev_fg
end)

-- Net
local netdowninfo = wibox.widget.textbox()
local netupinfo = lain.widget.net {
    settings = function ()
        widget:set_markup(markup.fontfg(theme.wibar_font, theme.colorscheme[9], " " .. net_now.sent .. "KB "))
        netdowninfo:set_markup(
            markup.fontfg(theme.wibar_font, theme.colorscheme[0], " " .. net_now.received .. "KB ")
        )
    end,
}

-- notification history
local notif_hist_icon = wibox.widget {
    widget = wibox.container.background,
    bg = nil,
    {
        layout = wibox.layout.stack,
        {
            widget = wibox.widget.textbox,
            markup = markup("#fff", "󰂚 "),
        },
        {
            id = "notification_badge",
            widget = wibox.container.margin,
            visible = false,
            margins = {
                bottom = 2,
                left = 2,
            },
            {
                widget = wibox.widget.textbox,
                markup = markup("#f00", ""),
                font = "JetBrainsMono Nerd Font 8",
            },
        },
    },
}
--  show unread notification on hover
local unread_popup = awful.popup {
    visible = false,
    ontop = true,
    widget = {
        layout = wibox.container.margin,
        margins = 3,
        {
            widget = wibox.widget.textbox,
            markup = markup(theme.colorscheme[6], "0") .. " new notification",
            font = "Roboto 10",
            id = "new_notification_counter",
        },
    },
}
function unread_popup:update_counter()
    local textbox = unread_popup.widget:get_children_by_id("new_notification_counter")[1]
    textbox.markup = textbox.markup:gsub("%d+</span", tostring(notif_widget.unread) .. "</span")
end

notif_hist_icon:buttons(
    gears.table.join(
        awful.button({}, awful.button.names.LEFT, nil, function ()
            unread_popup.visible = false
            notif_widget:toggle()
        end),
        awful.button({}, awful.button.names.RIGHT, nil, function ()
            unread_popup.visible = false
            notif_widget.unread = 0
            notif_hist_icon:get_children_by_id("notification_badge")[1].visible = false
        end)
    )
)
notif_hist_icon:connect_signal("mouse::enter", function ()
    unread_popup:update_counter()
    if notif_widget.unread < 1 then return end
    unread_popup:move_next_to(mouse.current_widget_geometry)
    unread_popup.visible = true
end)
notif_hist_icon:connect_signal("mouse::leave", function ()
    unread_popup.visible = false
end)

-- remove badge when the widget opened
naughty.connect_signal("notif_widget::opened", function ()
    notif_hist_icon:get_children_by_id("notification_badge")[1].visible = false
end)

-- show the badge when new notification appear
notif_man:connect_signal("added", function ()
    if notif_widget.visible then
        notif_widget.unread = 0
        return
    end
    notif_hist_icon:get_children_by_id("notification_badge")[1].visible = true
end)

local function at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake { app = awful.util.terminal }

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then wallpaper = wallpaper(s) end
    gears.wallpaper.maximized(wallpaper, s)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.custom_layout)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(my_table.join(
        awful.button({}, 1, function ()
            awful.layout.inc(1)
        end),
        awful.button({}, 2, function ()
            awful.layout.set(awful.layout.layouts[1])
        end),
        awful.button({}, 3, function ()
            awful.layout.inc(-1)
        end),
        awful.button({}, 4, function ()
            awful.layout.inc(1)
        end),
        awful.button({}, 5, function ()
            awful.layout.inc(-1)
        end)
    ))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        buttons = awful.util.taglist_buttons,
        widget_template = {
            id = "background_role",
            border_strategy = "inner",
            widget = wibox.container.background,
            {
                widget = wibox.layout.fixed.horizontal,
                fill_space = true,
                {
                    id = "icon_margin_role",
                    widget = wibox.container.margin,
                    {
                        id = "icon_role",
                        widget = wibox.widget.imagebox,
                        left = dpi(4),
                    },
                },
                {
                    id = "text_margin_role",
                    widget = wibox.container.margin,
                    left = dpi(4),
                    right = dpi(4),
                    {
                        id = "text_role",
                        widget = wibox.widget.textbox,
                    },
                },
            },
            create_callback = function (self, t, idx)
                self.fg = theme.colorscheme[t.index - 1]

                self:connect_signal("mouse::enter", function ()
                    if t.selected then return end
                    self.bg = theme.colorscheme[(idx - 1) % #theme.colorscheme]
                end)
                self:connect_signal("mouse::leave", function ()
                    if t.selected then return end
                    if t.urgent then
                        self.bg = theme.bg_urgent
                        return
                    end
                    self.bg = nil
                end)
            end,
            update_callback = function (self, t, idx)
                if t.selected then
                    theme.taglist_fg_focus = nil
                    theme.taglist_bg_focus = theme.colorscheme[t.index - 1]
                else
                    self.bg = nil
                    self.fg = theme.colorscheme[t.index - 1]
                end
            end,

        },
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = awful.util.tasklist_buttons,
        placement = awful.placement.center_vertical,
        layout = {
            max_widget_size = 200,
            spacing = 1,
            layout = wibox.layout.flex.horizontal,
        },
        widget_template = {
            {
                {
                    awful.widget.clienticon,
                    id     = "icon_margin_role",
                    left   = dpi(4),
                    widget = wibox.container.margin,
                },
                {
                    {
                        id     = "text_role",
                        widget = wibox.widget.textbox,
                    },
                    id     = "text_margin_role",
                    left   = dpi(4),
                    right  = dpi(4),
                    widget = wibox.container.margin,
                },
                fill_space = true,
                layout     = wibox.layout.fixed.horizontal,
            },
            id              = "background_role",
            widget          = wibox.container.background,
            create_callback = function (self, c, idx)
                -- self:connect_signal("mouse::enter", function()
                --     local c_idx = get_client_index(c, awful.screen.focused().selected_tags)
                --     self.bg = theme.colorscheme[c_idx % #theme.colorscheme] .. "dd"
                --     local txtbox = self:get_children_by_id("text_role")[1]
                --     txtbox.markup = markup("#000", txtbox.text)
                -- end)
                -- self:connect_signal("mouse::leave", function()
                --     if c.active then
                --         self.bg = theme.colorscheme[idx % #theme.colorscheme - 1]
                --     else
                --         self.bg = nil
                --         local txtbox = self:get_children_by_id("text_role")[1]
                --         txtbox.markup = markup(theme.colorscheme[idx % #theme.colorscheme - 1], txtbox.text)
                --     end
                -- end)
            end,
            update_callback = function (self, c, idx)
                if c.active then
                    theme.tasklist_bg_focus = theme.colorscheme[idx % #theme.colorscheme - 1]
                else
                    theme.tasklist_fg_normal = theme.colorscheme[idx % #theme.colorscheme - 1]
                end
            end,

        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar { position = "top", screen = s, height = dpi(18), bg = "#11111100", fg = theme.fg_normal }

    s.systray = wibox.widget.systray()

    local systray_toggle = wibox.widget {
        widget = wibox.container.background,
        bg = nil,
        {
            id = "systray_toggler",
            widget = wibox.widget.textbox("  "),
        },

    }

    systray_toggle:connect_signal("mouse::enter", function (self)
        self.fg = theme.colorscheme[0]
        self.bg = theme.bg_normal
    end)

    systray_toggle:connect_signal("mouse::leave", function (self)
        self.fg = theme.fg_normal
        self.bg = nil
    end)

    systray_toggle:get_children_by_id("systray_toggler")[1]:connect_signal("button::press", function (self, _, _, b)
        if b ~= 1 then return end

        if s.systray.visible then
            s.systray.visible = false
            self.text = "  "
        else
            s.systray.visible = true
            self.text = "  "
        end
    end)

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mylayoutbox,
            wibox.widget.textbox(" "),
            s.mytaglist,
            systray_toggle,
            s.systray,
            s.mypromptbox,
            -- mpdicon,
            -- theme.mpd.widget,
        },
        {
            -- Middle Widget
            layout = wibox.layout.align.horizontal,
            expand = "outside",
            nil,
            s.mytasklist,
            nil,
        },
        {
            -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --mailicon,
            --theme.mail.widget,
            netdowninfo,
            netupinfo.widget,
            pulseaudio_wdgt,
            temp.widget,
            cpu.widget,
            memory.widget,
            fs.widget,
            weather.widget,
            bat.widget,
            mytextclock,
            notif_hist_icon,
        },
    }

    -- -- Create the bottom wibox
    -- s.mybottomwibox = awful.wibar({ position = "bottom", screen = s, border_width = 0, height = dpi(20), bg = theme.bg_normal, fg = theme.fg_normal })

    -- -- Add widgets to the bottom wibox
    -- s.mybottomwibox:setup {
    --     layout = wibox.layout.align.horizontal,
    --     { -- Left widgets
    --         layout = wibox.layout.fixed.horizontal,
    --     },
    --     s.mytasklist, -- Middle widget
    --     { -- Right widgets
    --         layout = wibox.layout.fixed.horizontal,
    --         s.mylayoutbox,
    --     },
    -- }
end

return at_screen_connect
