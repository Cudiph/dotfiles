local function initialize()
    require("core.debug")
    require("core.misc")
    require("core.keybinds")
    require("core.signals")
    require("core.naughtyfication")
end

return {
    init = initialize,
}
