local gears         = require("gears")
local awful         = require("awful")
local beautiful     = require("beautiful")
local naughty       = require("naughty")
local lain          = require("lain")
local menubar       = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")
local switcher              = require("awesome-switcher")
local mytable               = awful.util.table or gears.table -- 4.{0,1} compatibility
local cfg_path              = gears.filesystem.get_configuration_dir()
local config                = require("config")

local notif_widget          = require("ui.notif_history")

local modkey                = config.modkey
local altkey                = config.altkey
local browser               = config.browser
local terminal              = config.terminal
local cycle_prev            = config.cycle_prev
local scrlocker             = config.scrlocker

-- {{{ Variable definitions

awful.util.taglist_buttons  = mytable.join(
    awful.button({}, 1, function (t)
        t:view_only()
    end),
    awful.button({ modkey }, 1, function (t)
        if client.focus then client.focus:move_to_tag(t) end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function (t)
        if client.focus then client.focus:toggle_tag(t) end
    end),
    awful.button({}, 4, function (t)
        awful.tag.viewprev(t.screen)
    end),
    awful.button({}, 5, function (t)
        awful.tag.viewnext(t.screen)
    end)
)

awful.util.tasklist_buttons = mytable.join(
    awful.button({}, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal("request::activate", "tasklist", { raise = true })
        end
    end),
    awful.button({}, 2, function (c)
        c:kill()
    end),
    awful.button({}, 3, function ()
        awful.menu.client_list { theme = { width = 250 } }
    end),
    awful.button({}, 4, function ()
        awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function ()
        awful.client.focus.byidx(-1)
    end)
)

-- {{{ Menu

-- {{{ Key bindings

local globalkeys            = mytable.join(
-- Destroy all notifications
    awful.key({ modkey }, "Escape", function ()
        naughty.destroy_all_notifications()
    end, { description = "destroy all notifications", group = "hotkeys" }),
    -- Windows like screenshot
    -- https://github.com/lcpz/dots/blob/master/bin/screenshot
    awful.key({ modkey }, "Print", function ()
        awful.spawn("flameshot full")
    end, { description = "take a screenshot", group = "hotkeys" }),
    awful.key({ modkey, "Shift" }, "s", function ()
        awful.spawn("flameshot gui")
    end, { description = "take a screenshot", group = "hotkeys" }),

    -- X screen locker
    awful.key({ altkey, "Control" }, "l", function ()
        os.execute(scrlocker)
    end, { description = "lock screen", group = "hotkeys" }),

    -- Show help
    awful.key({ modkey }, "s", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),

    -- Tag browsing
    awful.key({ modkey }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
    awful.key({ modkey }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),
    awful.key({ modkey }, ",", awful.tag.viewprev, { description = "view previous", group = "tag" }),
    awful.key({ modkey }, ".", awful.tag.viewnext, { description = "view next", group = "tag" }),
    awful.key({ modkey }, "grave", awful.tag.history.restore, { description = "go back", group = "tag" }),

    -- Non-empty tag browsing
    awful.key({ modkey, "Shift" }, "Left", function ()
        lain.util.tag_view_nonempty(-1)
    end, { description = "view  previous nonempty", group = "tag" }),
    awful.key({ modkey, "Shift" }, "Right", function ()
        lain.util.tag_view_nonempty(1)
    end, { description = "view  previous nonempty", group = "tag" }),
    awful.key({ modkey, "Shift" }, ",", function ()
        lain.util.tag_view_nonempty(-1)
    end, { description = "view  previous nonempty", group = "tag" }),
    awful.key({ modkey, "Shift" }, ".", function ()
        lain.util.tag_view_nonempty(1)
    end, { description = "view  previous nonempty", group = "tag" }),

    --[[
    awful.key({ altkey }, ",", function()
        lain.util.tag_view_nonempty(-1)
    end, { description = "view  previous nonempty", group = "tag" }),
    awful.key({ altkey }, ".", function()
        lain.util.tag_view_nonempty(1)
    end, { description = "view  previous nonempty", group = "tag" }),
    --]]

    -- Default client focus
    awful.key({ modkey, "Control" }, "j", function ()
        awful.client.focus.byidx(1)
    end, { description = "focus next by index", group = "client" }),
    awful.key({ modkey, "Control" }, "k", function ()
        awful.client.focus.byidx(-1)
    end, { description = "focus previous by index", group = "client" }),

    -- By-direction client focus
    awful.key({ modkey }, "j", function ()
        awful.client.focus.global_bydirection("down")
        if client.focus then client.focus:raise() end
    end, { description = "focus down", group = "client" }),
    awful.key({ modkey }, "k", function ()
        awful.client.focus.global_bydirection("up")
        if client.focus then client.focus:raise() end
    end, { description = "focus up", group = "client" }),
    awful.key({ modkey }, "h", function ()
        awful.client.focus.global_bydirection("left")
        if client.focus then client.focus:raise() end
    end, { description = "focus left", group = "client" }),
    awful.key({ modkey }, "l", function ()
        awful.client.focus.global_bydirection("right")
        if client.focus then client.focus:raise() end
    end, { description = "focus right", group = "client" }),

    -- Menu
    awful.key({ modkey }, "w", function ()
        awful.util.mymainmenu:show()
    end, { description = "show main menu", group = "awesome" }),

    -- Layout manipulation
    awful.key({ modkey, "Shift" }, "j", function ()
        awful.client.swap.byidx(1)
    end, { description = "swap with next client by index", group = "client" }),
    awful.key({ modkey, "Shift" }, "k", function ()
        awful.client.swap.byidx(-1)
    end, { description = "swap with previous client by index", group = "client" }),
    awful.key({ modkey, "Control" }, "j", function ()
        awful.screen.focus_relative(1)
    end, { description = "focus the next screen", group = "screen" }),
    awful.key({ modkey, "Control" }, "k", function ()
        awful.screen.focus_relative(-1)
    end, { description = "focus the previous screen", group = "screen" }),
    awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),
    -- default alt+tab (super+tab)
    awful.key({ modkey }, "Tab", function ()
        if cycle_prev then
            awful.client.focus.history.previous()
        else
            awful.client.focus.byidx(-1)
        end
        if client.focus then client.focus:raise() end
    end, { description = "cycle with previous/go back", group = "client" }),
    -- windows like alt+tab
    awful.key({ altkey }, "Tab", function ()
        switcher.switch(1, "Mod1", "Alt_L", "Shift", "Tab")
    end, { description = "cycle tab", group = "client" }),
    awful.key({ altkey, "Shift" }, "Tab", function ()
        switcher.switch(-1, "Mod1", "Alt_L", "Shift", "Tab")
    end, { description = "cycle tab (backward)", group = "client" }),

    -- Show/hide wibox
    awful.key({ modkey }, "b", function ()
        for s in screen do
            s.mywibox.visible = not s.mywibox.visible
            if s.mybottomwibox then s.mybottomwibox.visible = not s.mybottomwibox.visible end
        end
    end, { description = "toggle wibox", group = "awesome" }),

    -- Show/hide notification center
    awful.key({ modkey }, "\\", function ()
        notif_widget:toggle()
    end, { description = "toggle notififcation center", group = "awesome" }),

    -- On-the-fly useless gaps change
    awful.key({ altkey, "Control" }, "=", function ()
        lain.util.useless_gaps_resize(1)
    end, { description = "increment useless gaps", group = "tag" }),
    awful.key({ altkey, "Control" }, "-", function ()
        lain.util.useless_gaps_resize(-1)
    end, { description = "decrement useless gaps", group = "tag" }),

    -- Dynamic tagging
    awful.key({ modkey, "Shift" }, "n", function ()
        lain.util.add_tag()
    end, { description = "add new tag", group = "tag" }),
    awful.key({ modkey, "Shift" }, "r", function ()
        lain.util.rename_tag()
    end, { description = "rename tag", group = "tag" }),
    awful.key({ modkey, "Shift" }, "Left", function ()
        lain.util.move_tag(-1)
    end, { description = "move tag to the left", group = "tag" }),
    awful.key({ modkey, "Shift" }, "Right", function ()
        lain.util.move_tag(1)
    end, { description = "move tag to the right", group = "tag" }),
    awful.key({ modkey, "Shift" }, "d", function ()
        lain.util.delete_tag()
    end, { description = "delete tag", group = "tag" }),

    -- Standard program
    awful.key({ modkey }, "Return", function ()
        awful.spawn(terminal)
    end, { description = "open a terminal", group = "launcher" }),
    awful.key({ modkey, "Control" }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }),
    awful.key({ modkey, "Control" }, "Delete", awesome.quit, { description = "quit awesome", group = "awesome" }),
    awful.key({ modkey, altkey }, "l", function ()
        awful.tag.incmwfact(0.05)
    end, { description = "increase master width factor", group = "layout" }),
    awful.key({ modkey, altkey }, "h", function ()
        awful.tag.incmwfact(-0.05)
    end, { description = "decrease master width factor", group = "layout" }),
    awful.key({ modkey, "Shift" }, "h", function ()
        awful.tag.incnmaster(1, nil, true)
    end, { description = "increase the number of master clients", group = "layout" }),
    awful.key({ modkey, "Shift" }, "l", function ()
        awful.tag.incnmaster(-1, nil, true)
    end, { description = "decrease the number of master clients", group = "layout" }),
    awful.key({ modkey, "Control" }, "h", function ()
        awful.tag.incncol(1, nil, true)
    end, { description = "increase the number of columns", group = "layout" }),
    awful.key({ modkey, "Control" }, "l", function ()
        awful.tag.incncol(-1, nil, true)
    end, { description = "decrease the number of columns", group = "layout" }),
    awful.key({ modkey }, "space", function ()
        awful.layout.inc(1)
    end, { description = "select next", group = "layout" }),
    awful.key({ modkey, "Control" }, "space", function ()
        awful.layout.inc(-1)
    end, { description = "select previous", group = "layout" }),

    -- toggle stacking and tiling mode
    awful.key({ modkey, altkey }, "space", function ()
        local focused = awful.screen.focused().selected_tag
        if awful.layout.getname() ~= "floating" then
            awful.layout.set(awful.layout.suit.floating)
        else
            awful.layout.set(awful.layout.custom_layout[focused.index])
            if awful.layout.getname() == "floating" then awful.layout.set(awful.layout.layouts[1]) end
        end
    end, { description = "toggle tiling/floating", group = "layout" }),

    awful.key({ modkey, "Control" }, "n", function ()
        local c = awful.client.restore()
        -- Focus restored client
        if c then c:emit_signal("request::activate", "key.unminimize", { raise = true }) end
    end, { description = "restore minimized", group = "client" }),

    -- Dropdown application
    awful.key({ modkey }, "z", function ()
        awful.screen.focused().quake:toggle()
    end, { description = "dropdown application", group = "launcher" }),

    -- Screen brightness
    awful.key({}, "XF86MonBrightnessUp", function ()
        awful.spawn.easy_async_with_shell("brightnessctl s 3%+", function ()
            awesome.emit_signal("brightness::change")
        end)
    end),
    awful.key({}, "XF86MonBrightnessDown", function ()
        awful.spawn.easy_async_with_shell("brightnessctl s 3%-", function ()
            awesome.emit_signal("brightness::change")
        end)
    end),
    awful.key({}, "XF86ScreenSaver", function ()
        awful.spawn.with_shell("xset dpms force off")
    end),

    -- Audio Media Keys
    awful.key({}, "XF86AudioLowerVolume", function ()
        awesome.emit_signal("volume::change", "-3")
    end),
    awful.key({}, "XF86AudioRaiseVolume", function ()
        awesome.emit_signal("volume::change", "+3")
    end),
    awful.key({}, "XF86AudioMute", function ()
        awesome.emit_signal("volume::toggle")
    end),
    awful.key({}, "XF86AudioMicMute", function ()
        awful.spawn.easy_async_with_shell(cfg_path .. "scripts/toggle-mic.sh", function (out)
            naughty.notification {
                text = "Mic " .. string.sub(out, 1, -2),
            }
        end)
    end),

    -- Media Player Keys
    awful.key({}, "XF86AudioPlay", function ()
        awful.util.spawn("playerctl play-pause", false)
    end),
    awful.key({}, "XF86AudioNext", function ()
        awful.util.spawn("playerctl next", false)
    end),
    awful.key({}, "XF86AudioPrev", function ()
        awful.util.spawn("playerctl previous", false)
    end),
    awful.key({}, "XF86AudioStop", function ()
        awful.util.spawn("playerctl stop", false)
    end),

    -- Other Extra Keys
    awful.key({}, "XF86TouchpadToggle", function ()
        awful.spawn.easy_async_with_shell(cfg_path .. "scripts/toggle-touchpad.sh", function (out)
            naughty.notification {
                text = "Touchpad " .. string.sub(out, 1, -2),
            }
        end)
    end),
    awful.key({}, "XF86Calculator", function ()
        awful.spawn("octave --gui")
    end),
    awful.key({}, "XF86Mail", function ()
        awful.spawn("thunderbird -addressbook", {
            floating = true,
        })
    end),
    awful.key({}, "XF86Tools", function ()
        naughty.notification { text = "help" }
        awful.spawn("quodlibet --run", {
            floating = true,
        })
    end),
    awful.key({}, "XF86HomePage", function () --
        awful.spawn { "xdg-open", "https://wiki.archlinux.org" }
    end),
    awful.key({}, "XF86RFKill", function () --
        -- airplane mode is handled by systemd just like sleep button I think
        awful.spawn.easy_async_with_shell(cfg_path .. "scripts/toggle-rfkill.sh", function (out)
            naughty.notification {
                text = string.sub(out, 1, -2),
            }
        end)
    end),

    --[[ MPD control
    awful.key({ altkey, "Control" }, "Up",
        function ()
            awful.spawn.with_shell("mpc toggle")
            beautiful.mpd.update()
        end,
        {description = "mpc toggle", group = "widgets"}),
    awful.key({ altkey, "Control" }, "Down",
        function ()
            awful.spawn.with_shell("mpc stop")
            beautiful.mpd.update()
        end,
        {description = "mpc stop", group = "widgets"}),
    awful.key({ altkey, "Control" }, "Left",
        function ()
            awful.spawn.with_shell("mpc prev")
            beautiful.mpd.update()
        end,
        {description = "mpc prev", group = "widgets"}),
    awful.key({ altkey, "Control" }, "Right",
        function ()
            awful.spawn.with_shell("mpc next")
            beautiful.mpd.update()
        end,
        {description = "mpc next", group = "widgets"}),
    awful.key({ altkey }, "0",
        function ()
            local common = { text = "MPD widget ", position = "top_middle", timeout = 2 }
            if beautiful.mpd.timer.started then
                beautiful.mpd.timer:stop()
                common.text = common.text .. lain.util.markup.bold("OFF")
            else
                beautiful.mpd.timer:start()
                common.text = common.text .. lain.util.markup.bold("ON")
            end
            naughty.notification(common)
        end,
        {description = "mpc on/off", group = "widgets"}),
    --]]

    --[[ Copy primary to clipboard (terminals to gtk)
    awful.key({ modkey }, "c", function () awful.spawn.with_shell("xsel | xsel -i -b") end,
              {description = "copy terminal to gtk", group = "hotkeys"}),
    -- Copy clipboard to primary (gtk to terminals)
    awful.key({ modkey }, "v", function () awful.spawn.with_shell("xsel -b | xsel") end,
              {description = "copy gtk to terminal", group = "hotkeys"}),

    --]]
    awful.key({ modkey }, "v", function ()
        awful.spawn.with_shell("copyq toggle")
    end, { description = "copyq toggle menu", group = "hotkeys" }),

    -- User programs
    awful.key({ modkey, "Shift" }, "b", function ()
        awful.spawn(browser)
    end, { description = "run browser", group = "launcher" }),
    awful.key({ modkey }, "e", function ()
        awful.spawn("pcmanfm-qt")
    end, { description = "run file manager", group = "launcher" }),

    -- Menubar
    awful.key({ modkey }, "p", function ()
        menubar.show()
    end, { description = "show the menubar", group = "launcher" }),
    --]]

    --[[ dmenu
    awful.key({ modkey }, "x", function ()
            awful.spawn.with_shell(string.format("dmenu_run -i -fn 'Monospace' -nb '%s' -nf '%s' -sb '%s' -sf '%s'",
            beautiful.bg_normal, beautiful.fg_normal, beautiful.bg_focus, beautiful.fg_focus))
        end,
        {description = "show dmenu", group = "launcher"}),
    --]]
    -- alternatively use rofi, a dmenu-like application with more features
    -- check https://github.com/DaveDavenport/rofi for more details
    --[[ rofi
    awful.key({ modkey }, "x", function ()
            awful.spawn.with_shell(string.format("rofi -show %s -theme %s",
            'run', 'dmenu'))
        end,
        {description = "show rofi", group = "launcher"}),
    --]]
    -- Prompt
    awful.key({ modkey }, "r", function ()
        awful.screen.focused().mypromptbox:run()
    end, { description = "run prompt", group = "launcher" }),

    awful.key({ modkey }, "x", function ()
        awful.prompt.run {
            prompt = "Run Lua code: ",
            textbox = awful.screen.focused().mypromptbox.widget,
            exe_callback = awful.util.eval,
            history_path = awful.util.get_cache_dir() .. "/history_eval",
        }
    end, { description = "lua execute prompt", group = "awesome" }),
    --]]

    -- [[ Utility
    awful.key({ modkey, altkey }, "f", function ()
        awful.spawn("xcolor -s")
    end, { description = "color picker", group = "utility" }),

    awful.key({ modkey }, "q", function ()
        awful.spawn.easy_async_with_shell(cfg_path .. "scripts/toggle-autoscroll.sh", function (o)
            naughty.notification { title = "Mouse Autoscroll", text = o:sub(1, -2) }
        end)
    end, { description = "toggle mouse autoscrolling", group = "utility" })

--]]
)

local clientkeys            = mytable.join(
    awful.key({ modkey, "Shift" }, "m", lain.util.magnify_client, { description = "magnify client", group = "client" }),
    awful.key({ modkey }, "f", function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
    end, { description = "toggle fullscreen", group = "client" }),
    awful.key({ modkey, "Shift" }, "q", function (c)
        c:kill()
    end, { description = "close", group = "client" }),
    awful.key(
        { modkey, "Shift" },
        "space",
        awful.client.floating.toggle,
        { description = "toggle floating", group = "client" }
    ),
    awful.key({ modkey, "Control" }, "Return", function (c)
        c:swap(awful.client.getmaster())
    end, { description = "move to master", group = "client" }),
    awful.key({ modkey }, "o", function (c)
        c:move_to_screen()
    end, { description = "move to screen", group = "client" }),
    awful.key({ modkey }, "t", function (c)
        c.ontop = not c.ontop
    end, { description = "toggle keep on top", group = "client" }),
    awful.key({ modkey }, "n", function (c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
    end, { description = "minimize", group = "client" }),
    awful.key({ modkey }, "m", function (c)
        c.maximized = not c.maximized
        c:raise()
    end, { description = "(un)maximize", group = "client" }),
    awful.key({ modkey, "Control" }, "m", function (c)
        c.maximized_vertical = not c.maximized_vertical
        c:raise()
    end, { description = "(un)maximize vertically", group = "client" }),
    awful.key({ modkey, "Shift" }, "m", function (c)
        c.maximized_horizontal = not c.maximized_horizontal
        c:raise()
    end, { description = "(un)maximize horizontally", group = "client" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = mytable.join(
        globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9, function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then tag:view_only() end
        end, { description = "view tag #" .. i, group = "tag" }),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9, function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then awful.tag.viewtoggle(tag) end
        end, { description = "toggle tag #" .. i, group = "tag" }),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9, function ()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then client.focus:move_to_tag(tag) end
            end
        end, { description = "move focused client to tag #" .. i, group = "tag" }),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function ()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then client.focus:toggle_tag(tag) end
            end
        end, { description = "toggle focused client on tag #" .. i, group = "tag" })
    )
end

root.keys(globalkeys)

-- {{{ Mouse bindings

root.buttons(mytable.join(
    awful.button({}, 1, function ()
        awful.util.mymainmenu:hide()
    end),
    awful.button({}, 3, function ()
        awful.util.mymainmenu:toggle()
    end)
-- awful.button({}, 4, awful.tag.viewnext),
-- awful.button({}, 5, awful.tag.viewprev)
))

-- }}}

local clientbuttons = mytable.join(
    awful.button({}, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", { raise = true })
        awful.mouse.client.resize(c)
    end)
)

-- {{{ Rules

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen,
            size_hints_honor = false,
        },
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {
                "DTA",   -- Firefox addon DownThemAll.
                "copyq", -- Includes session name in class.
                "pinentry",
            },
            class = {
                "Arandr",
                "Blueman-manager",
                "Gpick",
                "discord",
                "Kruler",
                "MEGAsync",
                "MessageWin", -- kalarm.
                "Steam",
                "Sxiv",
                "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                "pcmanfm-qt",
                "Wpa_gui",
                "veromix",
                "Virt-manager",
                "xtightvncviewer",
            },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
                "Event Tester", -- xev.
            },
            role = {
                "AlarmWindow",   -- Thunderbird's calendar.
                "ConfigManager", -- Thunderbird's about:config.
                "pop-up",        -- e.g. Google Chrome's (detached) Developer Tools.
            },
        },
        properties = { floating = true },
    },

    -- Add titlebars to normal clients and dialogs
    { rule_any = { type = { "normal", "dialog" } }, properties = { titlebars_enabled = false } },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    {
        rule = { class = "Firefox" },
        properties = { screen = 1, tag = "2" },
    },
}
