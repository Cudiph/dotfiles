local lain          = require("lain")
local awful         = require("awful")
local menubar       = require("menubar")
local freedesktop   = require("freedesktop")
local hotkeys_popup = require("awful.hotkeys_popup")
require("awful.hotkeys_popup.keys")
local beautiful                        = require("beautiful")
local config                           = require("config")

local terminal                         = config.terminal
local editor                           = config.editor

awful.util.terminal                    = terminal
-- awful.util.tagnames                    = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
awful.util.tagnames                    = { "一", "二", "三", "四", "五", "六", "七", "八", "九" }
awful.layout.layouts                   = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.floating,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
    lain.layout.cascade,
    lain.layout.cascade.tile,
    lain.layout.centerwork,
    lain.layout.centerwork.horizontal,
    lain.layout.termfair,
    lain.layout.termfair.center,
}
awful.layout.custom_layout             = {
    lain.layout.centerwork,
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    lain.layout.termfair.center,
    awful.layout.suit.tile.left,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.magnifier,
    awful.layout.suit.floating,
    awful.layout.suit.floating,
}
-- floating wm
-- awful.layout.layouts = { awful.layout.suit.floating }

lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = 2
lain.layout.cascade.tile.offset_y      = 32
lain.layout.cascade.tile.extra_padding = 5
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2

-- Create a launcher widget and a main menu
local myawesomemenu                    = {
    {
        "Hotkeys",
        function ()
            hotkeys_popup.show_help(nil, awful.screen.focused())
        end,
    },
    { "Manual",      string.format("%s -e man awesome", terminal) },
    { "Edit config", string.format("%s -e %s %s", terminal, editor, awesome.conffile) },
    { "Restart",     awesome.restart },
    {
        "Quit",
        function ()
            awesome.quit()
        end,
    },
}

awful.util.mymainmenu                  = freedesktop.menu.build {
    before = {
        { "Awesome", myawesomemenu, beautiful.awesome_icon },
        -- other triads can be put here
    },
    after = {
        { "Open terminal", terminal },
        -- other triads can be put here
    },
}
-- Set the Menubar terminal for applications that require it
menubar.utils.terminal                 = terminal

-- }}}
