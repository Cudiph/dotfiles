local naughty     = require("naughty")
local wibox       = require("wibox")
local ruled       = require("ruled")
local gears       = require("gears")
local gtablejoin  = gears.table.join
local awful       = require("awful")
local abutton     = awful.button
local beautiful   = require("beautiful")
local cst         = require("naughty.constants")
local notif_list  = require("naughty.list.notifications")
local actionlist  = require("naughty.list.actions")
local wtitle      = require("naughty.widget.title")
local wmessage    = require("naughty.widget.message")
local wicon       = require("naughty.widget.icon")
local wbg         = require("naughty.container.background")
local clickable   = require("lib.widget").clickable_widget
local icon_lookup = require("menubar.utils").lookup_icon

local notif_saver = {}

local function process_notif(notif)
    if #notif_saver >= beautiful.notification_limit_displayed then
        notif_saver[1]:destroy(cst.notification_closed_reason.too_many_on_screen)
        table.remove(notif_saver, 1)
    end

    table.insert(notif_saver, notif)
end

ruled.notification.connect_signal("request::rules", function ()
    ruled.notification.append_rule {
        rule = { urgency = "critical" },
        properties = { border_color = beautiful.colorscheme[0], timeout = 0 },
    }

    ruled.notification.append_rule {
        rule = { urgency = "normal" },
        properties = { timeout = 20 },
    }

    ruled.notification.append_rule {
        rule = { urgency = "low" },
        properties = { border_color = beautiful.colorscheme[4], timeout = 10 },
    }
end)

naughty.connect_signal("request::icon", function (n, context, hints)
    if context ~= "app_icon" then return end

    local path = icon_lookup(hints.app_icon) or icon_lookup(hints.app_icon:lower())

    if path then n.icon = path end
end)

naughty.connect_signal("request::display_error", function (msg, startup)
    naughty.notification {
        title = msg,
        text = startup,
        icon = beautiful.awesome_icon,
    }
end)

naughty.connect_signal("request::display", function (notif)
    process_notif(notif)
    local action_button = wibox.widget {
        widget = actionlist,
        notification = notif,
        base_layout = wibox.widget {
            layout = wibox.layout.flex.horizontal,
            spacing = 2,
        },
        widget_template = {
            {
                {
                    -- {
                    --     id = "icon_role",
                    --     widget = wibox.widget.imagebox,
                    -- },
                    {
                        widget = wibox.container.margin,
                        margins = {
                            left = 5,
                            right = 5,
                            top = 4,
                            bottom = 4,
                        },
                        {
                            id = "text_role",
                            widget = wibox.widget.textbox,
                            halign = "center",
                        },
                    },
                    spacing = 0,
                    layout = wibox.layout.flex.horizontal,
                },
                id = "background_role",
                widget = wibox.container.background,
            },
            margins = 2,
            widget = wibox.container.margin,
            create_callback = function (self)
                local bg_role = self:get_children_by_id("background_role")[1]
                self:connect_signal("mouse::enter", function ()
                    bg_role.bg = beautiful.notification_action_bg_hover
                end)
                self:connect_signal("mouse::leave", function ()
                    bg_role.bg = beautiful.notification_action_bg_normal
                end)
            end,

        },
        style = {
            underline_normal = false,
            -- underline_selected = true,
            -- shape_normal = gears.shape.octogon,
            -- shape_selected = gears.shape.hexagon,
            -- shape_border_width_normal = 2,
            -- shape_border_width_selected = 4,
            -- icon_size_normal = 16,
            -- icon_size_selected = 24,
            -- shape_border_color_normal = "#0000ff",
            -- shape_border_color_selected = "#ff0000",
            -- bg_normal = "#ffff00",
            -- bg_selected = "#00ff00",
        },
    }

    clickable(action_button)

    notif.title =
        string.format("<span font = '" .. beautiful.notification_font_title .. "'><b>%s</b></span>", notif.title)

    local layoutbox = naughty.layout.box {
        notification = notif,
        type = "notification",
        shape = gears.shape.rectangle,
        widget_template = {
            {
                {
                    {
                        {
                            {
                                {
                                    wicon,
                                    {
                                        {
                                            wtitle,
                                            margins = {
                                                right = 15,
                                            },
                                            widget = wibox.container.margin,
                                        },
                                        wmessage,
                                        spacing = 0,
                                        layout = wibox.layout.fixed.vertical,
                                    },
                                    fill_space = true,
                                    spacing = 10,
                                    layout = wibox.layout.fixed.horizontal,
                                },
                                widget = wibox.container.margin,
                                margins = 5,
                            },
                            action_button,
                            layout = wibox.layout.fixed.vertical,
                        },
                        margins = beautiful.notification_margin,
                        widget = wibox.container.margin,
                    },
                    id = "background_role",
                    widget = wbg,
                },
                {
                    {
                        margins = {
                            top = beautiful.border_width + 1,
                            right = beautiful.border_width + 1,
                        },
                        {
                            widget = wibox.widget.imagebox,
                            forced_width = 15,
                            forced_height = 15,
                            image = icon_lookup(notif.app_name) or icon_lookup(notif.app_name:lower()),
                        },
                        widget = wibox.container.margin,
                    },
                    widget = wibox.container.place,
                    valign = "top",
                    halign = "right",
                },
                widget = wibox.layout.stack,
            },
            strategy = "max",
            width = beautiful.notification_max_width or beautiful.xresources.apply_dpi(500),
            widget = wibox.container.constraint,
        },
    }

    layoutbox:buttons {
        abutton({}, awful.button.names.LEFT, function ()
            if notif.run then notif:run() end
            notif:destroy()
        end),
        abutton({}, awful.button.names.RIGHT, function () notif:destroy() end),
    }
end)

-- -- Create a normal notification.
-- naughty.notification {
--     title = "A notification 1",
--     message = "This is very informative",
--     icon = beautiful.awesome_icon,
--     urgency = "normal",
-- }
--
-- -- Create a normal notification.
-- naughty.notification {
--     title = "A notification 2",
--     message = "This is very informative",
--     icon = beautiful.awesome_icon,
--     urgency = "critical",
--     icon_size = 200,
-- }
