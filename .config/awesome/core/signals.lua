local wibox = require("wibox")
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local beautiful   = require("beautiful")
local mytable     = awful.util.table or gears.table -- 4.{0,1} compatibility
local config      = require("config")
local naughty     = require("naughty")
local icon_lookup = require("menubar.utils").lookup_icon
local screen      = screen
local client      = client
local awesome     = awesome

local vi_focus    = config.vi_focus

-- {{{ Screen

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function (s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then wallpaper = wallpaper(s) end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end)

-- No borders when rearranging only 1 non-floating or maximized client
screen.connect_signal("arrange", function (s)
    local only_one = #s.tiled_clients == 1
    for _, c in pairs(s.clients) do
        if only_one and not c.floating or c.maximized or c.fullscreen then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width
        end
    end
end)

local bar = require("core.bar")
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function (s)
    bar(s)
end)

-- }}}

-- {{{ Signals

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- workaround for using custom icon for client
client.connect_signal("property::icon", function (c)
    if c.is_done then return end
    awful.spawn.easy_async_with_shell("sleep 0.00001", function ()
        local theme_icon = icon_lookup(c.class)
        if theme_icon then
            local surf = gears.surface.load(theme_icon)
            c.is_done = true
            c.icon = surf._native
        end
    end)
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function (c)
    -- Custom
    if beautiful.titlebar_fun then
        beautiful.titlebar_fun(c)
        return
    end

    -- Default
    -- buttons for the titlebar
    local buttons = mytable.join(
        awful.button({}, 1, function ()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.move(c)
        end),
        awful.button({}, 3, function ()
            c:emit_signal("request::activate", "titlebar", { raise = true })
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c, { size = 16 }):setup {
        {
            -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout = wibox.layout.fixed.horizontal,
        },
        {
            -- Middle
            {
                -- Title
                align = "center",
                widget = awful.titlebar.widget.titlewidget(c),
            },
            buttons = buttons,
            layout = wibox.layout.flex.horizontal,
        },
        {
            -- Right
            awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton(c),
            awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal(),
        },
        layout = wibox.layout.align.horizontal,
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function (c)
    c:emit_signal("request::activate", "mouse_enter", { raise = vi_focus })
end)

client.connect_signal("focus", function (c)
    c.border_color = beautiful.border_focus
    -- c.opacity = 1
end)
client.connect_signal("unfocus", function (c)
    c.border_color = beautiful.border_normal
    -- c.opacity = 1
end)

-- }}}

-- [[
-- Hide the menu when the mouse leaves it
awful.util.mymainmenu.wibox:connect_signal("mouse::leave", function ()
    if
        not awful.util.mymainmenu.active_child
        or (
            awful.util.mymainmenu.wibox ~= mouse.current_wibox
            and awful.util.mymainmenu.active_child.wibox ~= mouse.current_wibox
        )
    then
        awful.util.mymainmenu:hide()
    else
        awful.util.mymainmenu.active_child.wibox:connect_signal("mouse::leave", function ()
            if awful.util.mymainmenu.wibox ~= mouse.current_wibox then awful.util.mymainmenu:hide() end
        end)
    end
end)
--]]

-- [[ custom global signal

awesome.connect_signal("volume::change", function (value, is_from_slider)
    local cmd = string.format("pactl set-sink-volume @DEFAULT_SINK@ %s%%", value)
    awful.spawn.with_shell(cmd)
end)

awesome.connect_signal("volume::toggle", function ()
    awful.spawn.with_shell("pactl set-sink-mute @DEFAULT_SINK@ toggle")
end)

-- ]]
