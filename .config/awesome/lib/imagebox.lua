local wibox = require("wibox")
local gears = require("gears")

function factory(args)
    local image_box = wibox.widget(args)

    local function set_handle(ib, handle, cache)
        local dim = handle:get_dimensions()
        local is_handle_valid = dim.width > 0 and dim.height > 0
        if not is_handle_valid then return false end

        ib._private.default = { width = dim.width, height = dim.height }
        ib._private.handle = handle
        ib._private.cache = cache
        ib._private.image = nil

        return true
    end

    local function update_dpi(self, ctx)
        if not self._private.handle then return end

        local dpi = self._private.auto_dpi and ctx.dpi or self._private.dpi or nil

        local need_dpi = dpi and self._private.last_dpi ~= dpi

        local need_style = self._private.handle.set_stylesheet and self._private.stylesheet

        local old_size = self._private.default and self._private.default.width

        if dpi and dpi ~= self._private.cache.dpi then
            if type(dpi) == "table" then
                self._private.handle:set_dpi_x_y(dpi.x, dpi.y)
            else
                self._private.handle:set_dpi(dpi)
            end
        end

        if need_style and self._private.cache.stylesheet ~= self._private.stylesheet then
            self._private.handle:set_stylesheet(self._private.stylesheet)
        end

        -- Reload the size.
        if need_dpi or (need_style and self._private.stylesheet ~= self._private.last_stylesheet) then
            set_handle(self, self._private.handle, self._private.cache)
        end

        self._private.last_dpi = dpi
        self._private.cache.dpi = dpi
        self._private.last_stylesheet = self._private.stylesheet
        self._private.cache.stylesheet = self._private.stylesheet

        -- This can happen in the constructor when `dpi` is set after `image`.
        if old_size and old_size ~= self._private.default.width then
            self:emit_signal("widget::redraw_needed")
            self:emit_signal("widget::layout_changed")
        end
    end

    -- temporary implementation of https://github.com/awesomeWM/awesome/pull/3554
    function image_box:draw(ctx, cr, width, height)
        if width == 0 or height == 0 or not self._private.default then return end

        -- For valign = "top" and halign = "left"
        local translate = {
            x = 0,
            y = 0,
        }

        update_dpi(self, ctx)

        local w, h = self._private.default.width, self._private.default.height

        if self._private.resize then
            -- That's for the "fit" policy.
            local aspects = {
                w = width / w,
                h = height / h,
            }

            local policy = "fill"

            for _, aspect in ipairs { "w", "h" } do
                if self._private.upscale == false and (w < width and h < height) then
                    aspects[aspect] = 1
                elseif self._private.downscale == false and (w >= width and h >= height) then
                    aspects[aspect] = 1
                elseif policy == "none" then
                    aspects[aspect] = 1
                elseif policy == "vstretch" then
                    aspects.w = 1
                elseif policy == "hstretch" then
                    aspects.h = 1
                elseif policy == "fit" then
                    aspects[aspect] = math.min(width / w, height / h)
                elseif policy == "fill" then
                    if width / w > height / h then
                        aspects.h = aspects.w
                    else
                        aspects.w = aspects.h
                    end
                end
            end

            if self._private.halign == "center" then
                translate.x = math.floor((width - w * aspects.w) / 2)
            elseif self._private.halign == "right" then
                translate.x = math.floor(width - (w * aspects.w))
            end

            if self._private.valign == "center" then
                translate.y = math.floor((height - h * aspects.h) / 2)
            elseif self._private.valign == "bottom" then
                translate.y = math.floor(height - (h * aspects.h))
            end

            cr:translate(translate.x, translate.y)

            -- Before using the scale, make sure it is below the threshold.
            local threshold, max_factor = self._private.max_scaling_factor, math.max(aspects.w, aspects.h)

            if threshold and threshold > 0 and threshold < max_factor then
                aspects.w = (aspects.w * threshold) / max_factor
                aspects.h = (aspects.h * threshold) / max_factor
            end

            -- Set the clip
            if self._private.clip_shape then
                cr:clip(self._private.clip_shape(cr, w * aspects.w, h * aspects.h, unpack(self._private.clip_args)))
            end

            cr:scale(aspects.w, aspects.h)
        else
            if self._private.halign == "center" then
                translate.x = math.floor((width - w) / 2)
            elseif self._private.halign == "right" then
                translate.x = math.floor(width - w)
            end

            if self._private.valign == "center" then
                translate.y = math.floor((height - h) / 2)
            elseif self._private.valign == "bottom" then
                translate.y = math.floor(height - h)
            end

            cr:translate(translate.x, translate.y)

            -- Set the clip
            if self._private.clip_shape then
                cr:clip(self._private.clip_shape(cr, w, h, unpack(self._private.clip_args)))
            end
        end

        if self._private.handle then
            self._private.handle:render_cairo(cr)
        else
            cr:set_source_surface(self._private.image, 0, 0)

            local filter = self._private.scaling_quality

            if filter then cr:get_source():set_filter(cairo.Filter[filter:upper()]) end

            cr:paint()
        end
    end

    return image_box
end

return factory
