local function wrequire(t, k)
    return rawget(t, k) or require(t._NAME .. "." .. k)
end

local a = { _NAME = "lib" }

return setmetatable(a, { __index = wrequire })
