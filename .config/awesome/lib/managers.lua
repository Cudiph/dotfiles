-- export all manager in this file for easier import across other scripts

local lib         = require("lib")
local naughty_mgr = lib.naughty_manager:new()
local player_mgr  = lib.player_manager:new()

return {
    naughty_mgr = naughty_mgr,
    player_mgr = player_mgr,
}
