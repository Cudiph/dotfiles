local naughty = require("naughty")
-- very much useless

local Man = {
    _notif_list = nil, -- list of displayed notification back is newest notification
    _signal     = nil, -- list of saved signal function to call mapped by signal name
    limit       = 20,  -- limit saved notification
}

Man.__index = Man

function Man:get(idx)
    if #self._notif_list == 0 or idx > #self._notif_list then return nil end

    return self._notif_list[idx]
end

function Man:append(notif)
    if #self._notif_list >= self.limit then self:pop_front() end

    table.insert(self._notif_list, notif)
    self:emit_signal("added", notif)
end

function Man:pop_back()
    if #self._notif_list == 0 then return nil end

    local removed = table.remove(self._notif_list, #self._notif_list)
    self:emit_signal("removed", removed, #self._notif_list) -- removed notification and index of the removed notificaiton

    return removed
end

function Man:pop_front()
    if #self._notif_list == 0 then return nil end

    local removed = table.remove(self._notif_list, 1)
    self:emit_signal("removed", removed, 1) -- removed notification and index of the removed notificaiton

    return removed
end

function Man:emit_signal(name, ...)
    local signal_hash = self._signal[name]
    if not signal_hash then return end
    for _, v in pairs(signal_hash) do
        v(...)
    end
end

function Man:connect_signal(name, callback_fn)
    if not self._signal[name] then self._signal[name] = {} end

    table.insert(self._signal[name], callback_fn)
end

function Man:disconnect_signal(name, callback_fn)
    if not self._signal[name] then return end

    for i, fn in ipairs(self._signal[name]) do
        if fn == callback_fn then
            table.remove(self._signal[name], i)
            return
        end
    end
end

function Man:new(t)
    t = t or {}
    t._notif_list = {}
    t._signal = {}
    setmetatable(t, Man)

    naughty.connect_signal("request::display", function (notif)
        if notif.app_name == "awesomewm_widget" then return end
        notif.time = os.time()
        t:append(notif)
    end)

    return t
end

return Man
