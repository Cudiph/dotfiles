local wibox   = require("wibox")
local gears   = require("gears")
local naughty = require("naughty")

local slider  = wibox.widget.slider

function slider:set_value_without_emitting_signal(value)
    value = math.max(math.min(value, self:get_maximum()), self:get_minimum())
    local changed = self._private.value ~= value

    self._private.value = value

    if changed then self:emit_signal("widget::redraw_needed") end
end

return slider
