local function playerctl_metadata(mdat)
    if not mdat then return {} end
    local parsed = {}
    local splitted = {}

    for i in string.gmatch(mdat, "[^\n]+") do
        table.insert(splitted, i)
    end

    for _, str in pairs(splitted) do
        local k, val = string.match(str, "%g+%s+(%g+)%s+(.*)")
        if parsed[k] then
            parsed[k] = parsed[k] .. ", " .. val
        else
            parsed[k] = val
        end
    end
    parsed.app = string.match(splitted[1], "(%g+)%s")

    return parsed
end

local function microsec_to_sec(microsec)
    return microsec / 1000000
end

local function to_timestamp() end

return {
    playerctl_metadata = playerctl_metadata,
    microsec_to_sec = microsec_to_sec,
    to_timestamp = to_timestamp,
}
