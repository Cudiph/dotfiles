local lgi                 = require("lgi")
local playerctl           = lgi.Playerctl

local player_manager      = {
    _manager        = nil, -- the playerctl manager
    _player_pointer = 1,   -- index pointer to _players
    _players        = nil, -- list of players managed by this class sorted by first appearance
    -- players in playerctl manager is sorted by recent action to the player, the recent the top
    _signal         = nil, -- list of saved signal function to call mapped by signal name
    timer           = nil, -- list to store position timer mapped by player_name
}
-- player_manager.__index = player_manager
player_manager.__index    = function (tbl, key)
    local a = rawget(player_manager, key)
    if a then return a end

    return tbl._manager[key]
end

player_manager.__newindex = function (tbl, key, val)
    if key:sub(1, 3) == "on_" then tbl._manager[key] = val end

    player_manager[key] = val
end

-- proxy to manage_player
function player_manager:manage_player(player)
    self._manager:manage_player(player)
end

-- number relative to current number like 1 for next or -1 for prev
function player_manager:_set_player_pointer(n)
    local new_pos = self._player_pointer + n
    self._player_pointer = math.min(math.max(1, new_pos), #self._players)
    self:emit_signal("manager::player_change", self:get_active_player(), self._player_pointer, #self._players)
end

function player_manager:get_pointer_position()
    return self._player_pointer
end

function player_manager:next_player()
    if #self._manager.players == 0 then return false end
    self:_set_player_pointer(1)
    return true
end

function player_manager:prev_player()
    if #self._manager.players == 0 then return false end
    self:_set_player_pointer(-1)
    return true
end

function player_manager:get_active_player()
    if #self._manager.players == 0 then return nil end

    return self._players[self._player_pointer]
end

function player_manager:emit_signal(name, ...)
    local signal_hash = self._signal[name]
    if not signal_hash then return end
    for _, v in pairs(signal_hash) do
        v(...)
    end
end

function player_manager:connect_signal(name, callback_fn)
    if not self._signal[name] then self._signal[name] = {} end

    table.insert(self._signal[name], callback_fn)
end

function player_manager:disconnect_signal(name, callback_fn)
    if not self._signal[name] then return end

    for i, fn in ipairs(self._signal[name]) do
        if fn == callback_fn then
            table.remove(self._signal[name], i)
            return
        end
    end
end

function player_manager:set_active_player(player)
    for i, v in pairs(self._players) do
        if v == player then
            self._player_pointer = i
            self:emit_signal("manager::player_change", player, i, #self._players)
        end
    end
end

function player_manager:stop_timer_that_not_the_player_name(the_player_name)
    for k, v in pairs(self.timer) do
        if k ~= the_player_name then v:stop() end
    end
end

function player_manager:start_timer_by_player_name(the_player_name)
    if self.timer[the_player_name] and self.timer[the_player_name].started ~= true then
        self.timer[the_player_name]:start()
    end
end

function player_manager:stop_timer_by_player_name(the_player_name)
    self.timer[the_player_name]:stop()
end

--- player_names is ordered from active to last active
function player_manager:init_existing_player(fn, is_reverse)
    local player_names = self._manager.player_names
    if is_reverse then
        for i = #player_names, 1, -1 do
            fn(player_names[i])
        end
        return
    end

    for i = 1, #self._manager.player_names do
        fn(self._manager.player_names[i])
    end
end

-- function player_manager:get_player_by_name()
--     return self.manager.ge
-- end

-- the signal emitted is just the same functionality as signal from playerctl
-- except for manager::player_change that indicated the focused player change
-- note: must use with playerctl because it's mimic the behaviour of playerctld
function player_manager:new(t)
    t          = t or {}
    t._signal  = {}
    t._players = {}
    t.timer    = {}
    setmetatable(t, player_manager)
    t._manager = playerctl.PlayerManager()

    function t._manager.on_player_appeared(mgr, player)
        table.insert(t._players, 1, player)

        for _, v in pairs { "paused", "playing", "stopped" } do
            player.on_playback_status[v] = function (slef, status)
                t:emit_signal("player::playback_status::" .. v, slef, status)
            end
        end

        for _, v in pairs { "seeked", "metadata", "shuffle", "volume", "loop_status", "exit" } do
            player["on_" .. v] = function (...)
                t:emit_signal("player::" .. v, ...)
            end
        end

        for _, v in pairs {
            "playback_status::playing",
            "playback_status::paused",
            "seeked",
            "metadata",
            "shuffle",
            "volume",
            "loop_status",
        } do
            t:connect_signal("player::" .. v, function (slef)
                t._manager:move_player_to_top(slef)
                t:set_active_player(slef)
            end)
        end

        t:emit_signal("manager::player_appeared", mgr, player)
    end

    function t._manager.on_player_vanished(mgr, player)
        for i, v in pairs(t._players) do
            if v == player then
                table.remove(t._players, i)
                t:_set_player_pointer(-1)
            end
        end

        t:emit_signal("manager::player_vanished", mgr, player)
        t:emit_signal("manager::player_change", t._manager.players[1], t._player_pointer, #t._players)
    end

    function t._manager.on_name_appeared(mgr, name)
        t:emit_signal("manager::name_appeared", mgr, name)
    end

    function t._manager.on_name_vanished(mgr, name)
        t:emit_signal("manager::name_vanished", mgr, name)
    end

    return t
end

return player_manager
