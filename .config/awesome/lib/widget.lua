local capi = { mouse = mouse }

local function clickable_widget(wdgt)
    wdgt:connect_signal("mouse::enter", function ()
        local m = capi.mouse.current_wibox
        m.cursor = "hand1"
    end)

    wdgt:connect_signal("mouse::leave", function ()
        local m = capi.mouse.current_wibox
        if not m then return end
        m.cursor = "arrow"
    end)
end

return { clickable_widget = clickable_widget }
