-- {{{ Required libraries

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local gears     = require("gears")
local awful     = require("awful")
local beautiful = require("beautiful")
local ui        = require("ui")
local core      = require("core")
local cfg_path  = gears.filesystem.get_configuration_dir()


-- }}}

-- {{{ Autostart windowless processes

-- This function will run once every time Awesome is started
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
    end
end

run_once { "unclutter -root", "playerctld daemon" } -- comma-separated entries

-- This function implements the XDG autostart specification
--[[
awful.spawn.with_shell(
    'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
    'xrdb -merge <<< "awesome.started:true";' ..
    -- list each of your autostart commands, followed by ; inside single quotes, followed by ..
    'dex --environment Awesome --autostart --search-paths "${XDG_CONFIG_DIRS:=~/.config}/autostart:${XDG_CONFIG_HOME:=~}/autostart"' -- https://github.com/jceb/dex
)
--]]
-- }}}

-- [[ init
beautiful.init(os.getenv("HOME") .. "/.config/awesome/themes/init.lua")
core.init() -- awesomewm core config
ui.init()   -- ui

-- ]]

-- autorun app
awful.spawn.with_shell(cfg_path .. "scripts/autorun.sh")
