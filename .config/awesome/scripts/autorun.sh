#!/usr/bin/env bash

setup_mouse() {
	# MICE is at 2400 dpi from the official drivers
	MICE=(
		"SINOWEALTH Wired Gaming Mouse"
		"Logitech G304"
	)

	for MOUSE in "${MICE[@]}"; do
		# enable auto scroll with lock (on pressed) must change to 1
		#xinput --set-prop "$MOUSE" "libinput Button Scrolling Button Lock Enabled" 1

		# enable auto scroll feature
		#xinput --set-prop "$MOUSE" "libinput Scroll Method Enabled" 0 0 1

		# set flat mouse acceleration
		xinput --set-prop "pointer:$MOUSE" "libinput Accel Profile Enabled" 0 1

		# change sens using flat acceleration to prevent bug in xorg
		# ref: https://github.com/i3/i3/issues/3598
		xinput --set-prop "pointer:$MOUSE" "libinput Accel Speed" -0.75

	done

	# Enable tap to click
	xinput --set-prop "pointer:ELAN1200:00 04F3:30BA Touchpad" "libinput Tapping Enabled" 1
}

setup_xsettings() {
	xset r rate 400 30
}

run() {
	if ! pgrep -f "$1"; then
		"$@" &
	fi
}

APPS=(
	"picom --experimental-backends -b"
	"copyq"
	"fcitx5"
	"nm-applet"
	"playerctld daemon"
	"pulsedbus"
	"/lib/mate-polkit/polkit-mate-authentication-agent-1"
	"aria2tray --hide-window"
)

for i in "${APPS[@]}"; do
	run $i
done

setup_xsettings
setup_mouse
