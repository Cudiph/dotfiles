#!/usr/bin/env bash

MOUSE_IDS=$(xinput list --short | grep -E "slave\\s+pointer" | awk -F 'id=' '{print $2}' | sed -r 's/\s+.*//')

check_if_mouse_exist() {
	local REAL_MOUSE_IDS=""
	local PROP_LIST
	for i in $MOUSE_IDS; do
		IS_END_WITH_ONE=$(xinput list-props "$i" | grep -E 'libinput Scroll Methods Available.*, 1$')
		PROP_LIST="$PROP_LIST$IS_END_WITH_ONE"

		if [ "$IS_END_WITH_ONE" != "" ]; then
			REAL_MOUSE_IDS="$REAL_MOUSE_IDS $i"
		fi
	done

	echo $REAL_MOUSE_IDS | xargs

	if [ "$PROP_LIST" == '' ]; then
		return 63
	else
		return 0
	fi

}

REAL_MOUSE_IDS=$(check_if_mouse_exist)
if [ "$?" -eq 63 ]; then
	echo "empty"
	exit 63
fi

check_current_status() {
	local IS_ENABLED
	IS_ENABLED=$(xinput list-props $(echo "$REAL_MOUSE_IDS" | awk '{print $1}') | grep -E 'libinput Scroll Method Enabled \(' | awk '{print $NF}')

	if [[ $IS_ENABLED -eq 1 ]]; then
		echo on
	else
		echo off
	fi
}

STATUS=""
if [[ "$(check_current_status)" == "off" ]]; then
	for i in $REAL_MOUSE_IDS; do
		xinput set-prop "$i" 'libinput Scroll Method Enabled' 0, 0, 1
		STATUS="Activated"
	done
else
	for i in $REAL_MOUSE_IDS; do
		xinput set-prop "$i" 'libinput Scroll Method Enabled' 0, 0, 0
		STATUS="Deactivated"
	done
fi

echo $STATUS
