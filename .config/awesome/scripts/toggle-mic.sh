#!/usr/bin/env sh

DEFAULT_SAUCE=$(pactl get-default-source)

pactl set-source-mute "$DEFAULT_SAUCE" toggle

IS_MUTED=$(
	pactl get-source-mute "$DEFAULT_SAUCE" |
		gawk -F ' ' '{ print $NF }'
)

if [ "$IS_MUTED" = 'yes' ]; then
	echo "Muted"
else
	echo "Unmuted"
fi
