#!/usr/bin/env sh

sleep 0.5

BLOCKED=$(rfkill list | grep 'Soft blocked' | gawk '{print $NF}')
if [ "${BLOCKED:0:2}" == "no" ]; then
	echo "Airplane Mode Off"
else
	echo "Airplane Mode On"
fi
