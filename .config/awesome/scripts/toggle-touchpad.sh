#!/usr/bin/env sh

TOUCHPAD_ID="pointer:ELAN1200:00 04F3:30BA Touchpad"

T_ENABLED=$(
	xinput list-props "$TOUCHPAD_ID" |
		grep "Device Enabled" |
		gawk -F' ' '{ print $NF }'
)

if [ "$T_ENABLED" -eq '1' ]; then
	xinput disable "$TOUCHPAD_ID"
	echo "Disabled"
else
	xinput enable "$TOUCHPAD_ID"
	echo "Enabled"
fi
