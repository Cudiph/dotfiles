local dpi                             = require("beautiful.xresources").apply_dpi
local gears                           = require("gears")

local theme                           = {}

-- darkpriccio colorscheme
theme.colorscheme                     = {
    [0] = "#f08e97",
    "#a7e1a4",
    "#ffffa7",
    "#a5c0e1",
    "#c8a6e1",
    "#a1d0d4",
    "#f9b486",
    "#e1a5d7",
    "#b4b8e6",
    "#f8e0b4",
}
theme.confdir                         = os.getenv("HOME") .. "/.config/awesome/themes"
theme.icon_theme                      = "Papirus-Dark"
theme.wallpaper                       = theme.confdir .. "/background.jpg"
theme.font                            = "JetBrainsMono Nerd Font 9"
theme.menu_bg_normal                  = "#101713"
theme.menu_bg_focus                   = "#101713"
theme.bg_normal                       = "#101713"
theme.bg_focus                        = "#ccc"
theme.bg_urgent                       = "#fff"
theme.fg_normal                       = "#ccc"
theme.fg_focus                        = "#101713"
theme.fg_urgent                       = "#101713"
theme.fg_minimize                     = "#ccc"
theme.border_width                    = dpi(1)
theme.border_normal                   = "#423e44"
theme.border_focus                    = "#a7f1ff"
theme.border_marked                   = "#89b4f4"
theme.menu_border_width               = 0
theme.menu_width                      = dpi(130)
theme.menu_submenu_icon               = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                  = "#aaaaaa"
theme.menu_fg_focus                   = "#000"    -- "#ff8c00"
theme.menu_bg_normal                  = "#050505dd"
theme.menu_bg_focus                   = "#ffffff" -- "#050505dd"
theme.notification_action_bg_normal   = "#525753"
theme.notification_action_bg_hover    = "#2b322e"
theme.notification_action_bg_selected = "#525753"
theme.notification_action_fg_normal   = "#cccccc"
theme.notification_action_fg_selected = "#ffffff"
theme.notification_font               = "Roboto 10"
theme.notification_font_title         = "Roboto 11"
theme.notification_limit_displayed    = 10
theme.notification_limit_history      = 50
theme.notification_margin             = dpi(3)
theme.notification_max_width          = dpi(550)
theme.lain_icons                      = os.getenv("HOME") .. "/.config/awesome/lain/icons/layout/zenburn/"
theme.layout_termfair                 = theme.lain_icons .. "termfair.png"
theme.layout_centerfair               = theme.lain_icons .. "centerfair.png"  -- termfair.center
theme.layout_cascade                  = theme.lain_icons .. "cascade.png"
theme.layout_cascadetile              = theme.lain_icons .. "cascadetile.png" -- cascade.tile
theme.layout_centerwork               = theme.lain_icons .. "centerwork.png"
theme.layout_centerworkh              = theme.lain_icons .. "centerworkh.png" -- centerwork.horizontal
theme.taglist_fg_focus                = nil
theme.taglist_bg_focus                = nil
-- "linear:0,0:100,100:0,#f08e97:0.1,#a7e1a4:0.2,#ffffa7:0.3,#a5c0e1:0.4,#c8a6e1:0.5,#a1d0d4:0.6,#f9b486:0.7,#e1a5d7:0.8,#b4b8e6:0.9,#b4b8e6:1.0,#f8e0b4"
theme.taglist_font                    = "JetBrainsMono Nerd Font Bold 9"
theme.taglist_shape                   = gears.shape.rectangle
theme.taglist_spacing                 = 3
theme.taglist_squares_sel             = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel           = theme.confdir .. "/icons/square_b.png"
theme.tasklist_font                   = "JetBrainsMono Nerd Font Bold 10"
theme.tasklist_plain_task_name        = false
theme.tasklist_disable_icon           = false
theme.tasklist_shape                  = gears.shape.rectangle
theme.tasklist_bg_normal              = "#00000100"
theme.tasklist_fg_focus               = "#000000"
theme.tasklist_bg_focus               =
"linear:0,0:100,100:0,#f08e97:0.1,#a7e1a4:0.2,#ffffa7:0.3,#a5c0e1:0.4,#c8a6e1:0.5,#a1d0d4:0.6,#f9b486:0.7,#e1a5d7:0.8,#b4b8e6:0.9,#b4b8e6:1.0,#f8e0b4"
theme.useless_gap                     = dpi(2)
theme.awesome_icon                    = theme.confdir .. "/icons/awesome.png"
theme.layout_tile                     = theme.confdir .. "/mono_icons/tile.png"
theme.layout_tileleft                 = theme.confdir .. "/mono_icons/tileleft.png"
theme.layout_tilebottom               = theme.confdir .. "/mono_icons/tilebottom.png"
theme.layout_tiletop                  = theme.confdir .. "/mono_icons/tiletop.png"
theme.layout_fairv                    = theme.confdir .. "/mono_icons/fairv.png"
theme.layout_fairh                    = theme.confdir .. "/mono_icons/fairh.png"
theme.layout_spiral                   = theme.confdir .. "/mono_icons/spiral.png"
theme.layout_dwindle                  = theme.confdir .. "/mono_icons/dwindle.png"
theme.layout_max                      = theme.confdir .. "/mono_icons/max.png"
theme.layout_fullscreen               = theme.confdir .. "/mono_icons/fullscreen.png"
theme.layout_magnifier                = theme.confdir .. "/mono_icons/magnifier.png"
theme.layout_floating                 = theme.confdir .. "/mono_icons/floating.png"
theme.wibar_font                      = "JetBrainsMono Nerd Font Bold 9"

return theme
