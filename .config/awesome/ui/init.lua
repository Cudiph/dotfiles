function initialize()
    require("ui.player")
    require("ui.notif_history")
    require("ui.naughty_brightness")
    require("ui.naughty_volume")
end

local function wrequire(t, k)
    return rawget(t, k) or require(t._NAME .. "." .. k)
end

local ui = {
    init = initialize,
    _NAME = "ui",
}

return setmetatable(ui, { __index = wrequire })
