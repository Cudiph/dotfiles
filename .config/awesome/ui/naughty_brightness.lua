-- Screen brightness popup
local awful        = require("awful")
local wibox        = require("wibox")
local gears        = require("gears")
local theme        = require("beautiful")
local awesome      = awesome
local screen       = screen
local mouse        = mouse

local nosig_slider = require("lib.nosig_slider")

local timer        = nil
local bar_height   = 7
local slider       = nosig_slider {
    minimum = 0,
    maximum = 100,
    value = 0,
    bar_shape = gears.shape.rounded_bar,
    bar_height = bar_height,
    bar_color = "#1b1f27",
    bar_active_color = theme.colorscheme[2],
    handle_color = theme.colorscheme[2],
    handle_shape = gears.shape.circle,
    handle_width = bar_height,
    forced_width = 200,
    forced_height = bar_height,
}

local popup        = awful.popup {
    hide_on_right_click = false,
    widget = {
        widget = wibox.container.background,
        bg = "#ffffff00",
        {

            widget = wibox.container.margin,
            margins = {
                left = 10,
                right = 10,
            },
            {
                widget = wibox.layout.fixed.horizontal,
                spacing = 5,
                {
                    widget = wibox.widget.textbox,
                    markup = "󰃟 ",
                    font = "14",
                },
                slider,

            },
        },
    },
    placement = function (self)
        local screen_height = screen[mouse.screen].geometry.height
        awful.placement.bottom(self, { margins = { bottom = screen_height / 8 } })
    end,
    shape = gears.shape.rounded_bar,
    visible = false,
    ontop = true,
}


slider:connect_signal("property::value", function (_, val)
    awful.spawn.easy_async_with_shell(string.format("brightnessctl s %s", val))
    if timer then timer:again() end
end)

local function on_brightness_change()
    popup.visible = true
    awful.spawn.easy_async_with_shell("brightnessctl i", function (o)
        local current_brightness = tonumber(string.match(o, "Current brightness:.*%((%d+)%%%)"))
        slider:set_value_without_emitting_signal(current_brightness)

        if timer then
            timer:again()
            return
        end

        timer = gears.timer {
            timeout = 3,
            single_shot = true,
            callback = function ()
                popup.visible = false
            end,
        }
        timer:start()
    end)
end

awesome.connect_signal("brightness::change", function ()
    on_brightness_change()
end)
