-- Audio volume popup
local awful        = require("awful")
local wibox        = require("wibox")
local gears        = require("gears")
local theme        = require("beautiful")
local awesome      = awesome
local screen       = screen
local mouse        = mouse

local nosig_slider = require("lib.nosig_slider")
local cmd          = "pactl"
local channel      = "@DEFAULT_SINK@"

local info_cmd     = string.format("%s get-sink-volume %s; pactl get-sink-mute %s", cmd, channel, channel)

local timer        = nil
local bar_height   = 7
local icon         = wibox.widget.textbox("󰕾 ")
icon.font          = "14"
local slider       = nosig_slider {
    minimum = 0,
    maximum = 100,
    value = 0,
    bar_shape = gears.shape.rounded_bar,
    bar_height = bar_height,
    bar_color = "#1b1f27",
    bar_active_color = theme.colorscheme[5],
    handle_color = theme.colorscheme[5],
    handle_shape = gears.shape.circle,
    handle_width = bar_height,
    forced_width = 200,
    forced_height = bar_height,
}
local popup        = awful.popup {
    hide_on_right_click = false,
    widget = {
        widget = wibox.container.background,
        bg = "#ffffff00",
        {

            widget = wibox.container.margin,
            margins = {
                left = 10,
                right = 10,
            },
            {
                widget = wibox.layout.fixed.horizontal,
                spacing = 5,
                icon,
                slider,

            },
        },
    },
    placement = function (self)
        local screen_height = screen[mouse.screen].geometry.height
        awful.placement.bottom(self, { margins = { bottom = screen_height / 8 } })
    end,
    shape = gears.shape.rounded_bar,
    visible = false,
    ontop = true,
}

slider:connect_signal("property::value", function (_, val)
    awful.spawn.with_shell(string.format("pactl set-sink-volume @DEFAULT_SINK@ %s%%", val))
    if timer then timer:again() end
end)

local function on_volume_change()
    popup.visible = true
    awful.spawn.easy_async_with_shell(info_cmd, function (o)
        local level, muted = string.match(o, "([%d]+)%%.*Mute: ([%l]+)")
        slider:set_value_without_emitting_signal(tonumber(level))

        if muted == "yes" then
            icon.markup = "󰝟 "
        else
            icon.markup = "󰕾 "
        end

        if timer then
            timer:again()
            return
        end

        timer = gears.timer {
            timeout = 3,
            single_shot = true,
            callback = function ()
                popup.visible = false
            end,
        }
        timer:start()
    end)
end

awesome.connect_signal("volume::change", function ()
    on_volume_change()
end)

awesome.connect_signal("volume::toggle", function ()
    if timer then timer:again() end

    awful.spawn.easy_async_with_shell(info_cmd, function (o)
        local _, muted = string.match(o, "([%d]+)%%.*Mute: ([%l]+)")
        if muted == "yes" then
            icon.markup = "󰝟 "
        else
            icon.markup = "󰕾 "
        end
    end)
end)
