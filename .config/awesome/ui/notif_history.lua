local awful                  = require("awful")
local wibox                  = require("wibox")
local base                   = wibox.widget.base
local gears                  = require("gears")
local beautiful              = require("beautiful")
local theme                  = require("beautiful")
local naughty                = require("naughty")
local actionlist             = require("naughty.list.actions")
local wtitle                 = require("naughty.widget.title")
local wmessage               = require("naughty.widget.message")
local wicon                  = require("naughty.widget.icon")
local wbg                    = require("naughty.container.background")
local icon_lookup            = require("menubar.utils").lookup_icon
local lain                   = require("lain")
local markup                 = lain.util.markup
local lib                    = require("lib")
local dpi                    = require("beautiful.xresources").apply_dpi

local screen                 = screen
local mouse                  = mouse

local notif_man              = lib.managers.naughty_mgr

local screen_height          = screen[mouse.screen].geometry.height

local root_container         = wibox {
    ontop = true,
    width = dpi(400),
    height = screen_height - dpi(24),
    bg = "#000000",
    visible = false,
    widget = {
        widget = wibox.container.margin,
        margins = 10,
        {
            layout = wibox.layout.fixed.vertical,
            {
                widget = wibox.container.margin,
                margins = {
                    bottom = 10,
                },
                {
                    layout = wibox.layout.stack,
                    {
                        layout = wibox.layout.align.horizontal,
                        {
                            widget = wibox.container.background,
                            shape = gears.shape.circle,
                            id = "clear_button",
                            {

                                widget = wibox.widget.textbox,
                                markup = markup(theme.colorscheme[3], "  "),
                                font = "JetBrainsMono Nerd Font 16",
                            },
                        },
                        nil,
                        {
                            widget = wibox.container.background,
                            shape = gears.shape.circle,
                            id = "close_button",
                            {
                                widget = wibox.widget.textbox,
                                markup = markup(theme.colorscheme[0], " 󱎘 "),
                                font = "JetBrainsMono Nerd Font 16",
                            },
                        },
                    },
                    {

                        widget = wibox.widget.textbox,
                        markup = markup(theme.colorscheme[7], "<b>NOTIFICATION HISTORY [ 0 ]</b>"),
                        align = "center",
                        font = "Roboto 12",
                        id = "title_and_counter",
                    },

                },
            },
        },
    },
}
root_container.unread        = 0

local notif_container        = wibox.widget {
    layout = wibox.layout.fixed.vertical,
    fill_space = false, -- must false
    visible = true,
    spacing = 10,
}
notif_container.max_offset_y = 0

local root_notif_container   = wibox {
    ontop = true,
    width = dpi(400),
    height = screen_height - dpi(68),
    bg = "#000000",
    visible = false,
    widget = {
        widget = wibox.container.margin,
        margins = 10,
        notif_container,
    },
}

awful.placement.bottom_right(root_container, {
    margins = {
        top = dpi(3),
        right = dpi(3),
        bottom = dpi(3),
    },
})

awful.placement.bottom_right(root_notif_container, {
    margins = {
        top = dpi(3),
        right = dpi(3),
        bottom = dpi(3),
    },
})

notif_man:connect_signal("added", function (notif)
    root_container.unread = root_container.unread + 1
    local action_button = wibox.widget {
        widget = actionlist,
        notification = notif,
        base_layout = wibox.widget {
            layout = wibox.layout.flex.horizontal,
            spacing = 2,
        },
        widget_template = {
            {
                {
                    {
                        widget = wibox.container.margin,
                        margins = {
                            left = 5,
                            right = 5,
                            top = 4,
                            bottom = 4,
                        },
                        {
                            id = "text_role",
                            widget = wibox.widget.textbox,
                            halign = "center",
                        },
                    },
                    spacing = 0,
                    layout = wibox.layout.flex.horizontal,
                },
                id = "background_role",
                widget = wibox.container.background,
            },
            margins = 2,
            widget = wibox.container.margin,
            create_callback = function (self)
                local bg_role = self:get_children_by_id("background_role")[1]
                self:connect_signal("mouse::enter", function ()
                    bg_role.bg = beautiful.notification_action_bg_hover
                end)
                self:connect_signal("mouse::leave", function ()
                    bg_role.bg = beautiful.notification_action_bg_normal
                end)
            end,

        },
        style = {
            underline_normal = false,
        },
    }

    notif.title =
        string.format("<span font = '" .. beautiful.notification_font_title .. "'><b>%s</b></span>", notif.title)

    local nwdg = wibox.widget {
        {
            {
                {
                    {
                        {
                            {
                                { widget = wicon, notification = notif },
                                {
                                    {
                                        { widget = wtitle, notification = notif },
                                        margins = {
                                            right = 15,
                                        },
                                        widget = wibox.container.margin,
                                    },
                                    {
                                        widget = wmessage,
                                        notification = notif,
                                        visible = true,
                                        id = "notification_message_short",
                                    },
                                    {
                                        widget = wibox.widget.textbox,
                                        markup = notif.text,
                                        font = theme.notification_font,
                                        visible = false,
                                        id = "notification_message_long",
                                    },
                                    spacing = 0,
                                    layout = wibox.layout.fixed.vertical,
                                },
                                fill_space = true,
                                spacing = 10,
                                layout = wibox.layout.fixed.horizontal,
                            },
                            widget = wibox.container.margin,
                            margins = 5,
                        },
                        { widget = action_button, notification = notif },
                        layout = wibox.layout.fixed.vertical,
                    },
                    margins = beautiful.notification_margin,
                    widget = wibox.container.margin,
                },
                id = "background_role",
                widget = wbg,
                notification = notif,
            },
            {
                {
                    margins = {
                        top = beautiful.border_width + 1,
                        right = beautiful.border_width + 1,
                    },
                    {
                        widget = wibox.widget.imagebox,
                        forced_width = 15,
                        forced_height = 15,
                        image = icon_lookup(notif.app_name) or icon_lookup(notif.app_name:lower()),
                    },
                    widget = wibox.container.margin,
                },
                widget = wibox.container.place,
                valign = "top",
                halign = "right",
            },
            {
                {
                    {

                        {

                            widget = wibox.widget.textbox,
                            markup = "<b>" .. os.date(
                                markup(theme.colorscheme[0], "%H:%M") .. markup(theme.colorscheme[1], " %x"),
                                notif.time) .. "</b>",
                            visible = false,
                            id = "notification_time",
                        },
                        widget = wibox.container.background,
                        bg = "#000",
                    },
                    widget = wibox.container.margin,
                    margins = {
                        top = beautiful.border_width + 1,
                        left = beautiful.border_width + 3,
                    },
                },
                widget = wibox.container.place,
                valign = "top",
                halign = "left",
            },
            ontop = true,
            widget = wibox.layout.stack,
        },
        strategy = "max",
        height = beautiful.xresources.apply_dpi(150),
        widget = wibox.container.constraint,
    }

    nwdg.buttons = {
        awful.button({}, awful.button.names.LEFT, nil, function (a)
            local this = a.widget
            local short_msg = nwdg:get_children_by_id("notification_message_short")[1]
            local long_msg = nwdg:get_children_by_id("notification_message_long")[1]
            if short_msg.visible then
                short_msg.visible = false
                long_msg.visible = true
                this.prev_height = this.height
                this.height = nil
            else
                short_msg.visible = true
                long_msg.visible = false
                this.height = this.prev_height
            end
        end),
        awful.button({}, awful.button.names.RIGHT, nil, function (a)
            local this = a.widget
            notif_container:remove_widgets(this)
        end),
    }

    nwdg:connect_signal("mouse::enter", function ()
        local time_gadget = nwdg:get_children_by_id("notification_time")[1]
        time_gadget.visible = true
    end)

    nwdg:connect_signal("mouse::leave", function ()
        local time_gadget = nwdg:get_children_by_id("notification_time")[1]
        time_gadget.visible = false
    end)

    if #notif_container.children >= theme.notification_limit_history then
        notif_container:remove(#notif_container.children)
    end

    notif_container:insert(1, nwdg)
end)

-- button to close the widget
local close_button = root_container:get_children_by_id("close_button")[1]
close_button.buttons = {
    awful.button({}, awful.button.names.LEFT, function (a)
        root_container:close()
    end),
}

close_button:connect_signal("mouse::enter", function (this)
    this.bg = "#333"
end)

close_button:connect_signal("mouse::leave", function (this)
    this.bg = nil
end)

-- button to delete all notification history
local clear_button = root_container:get_children_by_id("clear_button")[1]
clear_button:connect_signal("button::press", function ()
    notif_container:reset()
end)

clear_button:connect_signal("mouse::enter", function (this)
    this.bg = "#333"
end)

clear_button:connect_signal("mouse::leave", function (this)
    this.bg = nil
end)

notif_container:connect_signal("widget::layout_changed", function ()
    local title = root_container:get_children_by_id("title_and_counter")[1]
    title.markup = title.markup:gsub("%d+", tostring(#notif_container.children))
end)

-- handle widget visibility
function root_container:open()
    root_container.unread = 0
    notif_container.offset_y = 0 -- reset scrolling
    root_container.visible = true
    root_notif_container.visible = true
    naughty.emit_signal("notif_widget::opened")
    notif_container:emit_signal("widget::layout_changed")
end

function root_container:close()
    root_notif_container.visible = false
    root_container.visible = false
    naughty.emit_signal("notif_widget::closed")
end

function root_container:toggle()
    if root_container.visible then
        root_container:close()
    else
        root_container:open()
    end
end

-- handle scrolling
-- offset_y is offset of y axis to scrolling
-- height param is also used to determine the view range
-- drawing all the notification will make scrolling so slow, to optimize this we can
-- render only the widget that appear in the screen. So if the y offset is at 120,
-- then render all widget which in range 120 + height y pixel.
function notif_container:layout(context, width, height)
    local result = {}
    local spacing = self._private.spacing or 0
    local is_y = self._private.dir == "y"
    local is_x = not is_y
    local abspace = math.abs(spacing)
    local spoffset = spacing < 0 and 0 or spacing
    local widgets_nr = #self._private.widgets
    local spacing_widget
    local x, y = 0, 0

    spacing_widget = spacing ~= 0 and self._private.spacing_widget or nil

    for index, widget in pairs(self._private.widgets) do
        if y > (notif_container.offset_y + height) then break end
        local w, h, local_spacing = width - x, height, spacing

        -- Some widget might be zero sized either because this is their
        -- minimum space or just because they are really empty. In this case,
        -- they must still be added to the layout. Otherwise, if their size
        -- change and this layout is resizable, they are lost "forever" until
        -- a full relayout is called on this fixed layout object.
        local zero = false

        if is_y then
            if index ~= widgets_nr or not self._private.fill_space then
                h = select(2, base.fit_widget(self, context, widget, w, h))
                zero = h == 0
            end

            if y - spacing >= height then
                -- pop the spacing widget added in previous iteration if used
                if spacing_widget then
                    table.remove(result)

                    -- Avoid adding zero-sized widgets at an out-of-bound
                    -- position.
                    y = y - spacing
                end
            end
        end

        if zero then
            local_spacing = 0
        end

        -- place widget if in viewable range
        if (h + y) >= notif_container.offset_y then
            table.insert(result, base.place_widget_at(widget, x, y - notif_container.offset_y, w, h))
        end

        x = is_x and x + w + local_spacing or x
        y = is_y and y + h + local_spacing or y

        -- Add the spacing widget (if needed)
        if index < widgets_nr and spacing_widget then
            table.insert(result, base.place_widget_at(
                spacing_widget,
                is_x and (x - spoffset) or x,
                is_y and (y - spoffset) or y,
                is_x and abspace or w,
                is_y and abspace or h
            ))
        end
        self.max_offset_y = y - height / 2
    end

    return result
end

notif_container.buttons = {
    awful.button({}, awful.button.names.SCROLL_UP, function ()
        notif_container.offset_y = notif_container.offset_y - 60 < 0 and 0 or notif_container.offset_y - 60
        notif_container:emit_signal("widget::layout_changed")
    end),
    awful.button({}, awful.button.names.SCROLL_DOWN, function ()
        notif_container.offset_y = notif_container.offset_y + 60 > notif_container.max_offset_y and
            notif_container.offset_y or notif_container.offset_y + 60
        notif_container:emit_signal("widget::layout_changed")
    end),
}

return root_container
