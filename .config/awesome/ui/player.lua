local awful              = require("awful")
local wibox              = require("wibox")
local gears              = require("gears")
local theme              = require("beautiful")
local naughty            = require("naughty")
local lain               = require("lain")
local icon_lookup        = require("menubar.utils").lookup_icon
local parser             = require("lib.parser")
local lgi                = require("lgi")
local lib                = require("lib")
local dpi                = require("beautiful.xresources").apply_dpi
local cairo              = lgi.cairo
local playerctl          = lgi.Playerctl
local mdat_parser        = parser.playerctl_metadata
local markup             = lain.util.markup
local clickable          = lib.widget.clickable_widget

-- get current controlled player by getting the top of players prpo
local player_mgr         = lib.managers.player_mgr

local cont_width         = dpi(400)
local cont_height        = dpi(165)

local bg_image           = lib.imagebox {
    image = nil,
    resize = true,
    shape = gears.shape.rounded_rect,
    widget = wibox.widget.imagebox,
}

local prev_song_button   = wibox.widget {
    {
        widget = wibox.container.background,
        shape = gears.shape.circle,
        bg = "#01010100",
        {
            widget = wibox.container.margin,
            margins = 3,
            {
                widget = wibox.widget.textbox,
                font = "JetBrainsMono 18",
                markup = markup(theme.colorscheme[6], "󰒮"),
            },
        },
    },
    layout = wibox.layout.flex.horizontal,
}

local next_song_button   = wibox.widget {
    {
        widget = wibox.container.background,
        shape = gears.shape.circle,
        bg = "#01010100",
        {
            widget = wibox.container.margin,
            margins = 3,
            {
                widget = wibox.widget.textbox,
                font = "JetBrainsMono 18",
                markup = markup(theme.colorscheme[6], "󰒭"),
            },
        },
    },
    layout = wibox.layout.flex.horizontal,
}

local play_button        = wibox.widget {
    widget = wibox.container.background,
    shape = gears.shape.circle,
    bg = "#000",
    {
        widget = wibox.container.margin,
        margins = 11,
        {
            widget = wibox.widget.textbox,
            font = "JetBrainsMono 20",
            markup = markup(theme.colorscheme[4], ""),
            id = "play_pause_icon",
        },
    },
}

local prev_player_button = wibox.widget {
    widget = wibox.container.background,
    shape = gears.shape.circle,
    bg = "#01010166",
    {
        widget = wibox.widget.textbox,
        font = "JetBrainsMono 14",
        markup = markup(theme.colorscheme[0], ""),
    },
}

local next_player_button = wibox.widget {
    widget = wibox.container.background,
    shape = gears.shape.circle,
    bg = "#01010166",
    {
        widget = wibox.widget.textbox,
        font = "JetBrainsMono 14",
        markup = markup(theme.colorscheme[0], ""),
    },
}

local seek_slider        = lib.nosig_slider {
    minimum = 0,
    maximum = 100,
    value = 50,
    bar_shape = gears.shape.rounded_bar,
    bar_height = 2,
    bar_color = "#aaaaaa",
    bar_active_color = theme.colorscheme[2],
    handle_color = theme.colorscheme[2],
    handle_shape = gears.shape.circle,
    handle_width = 8,
    forced_width = cont_width - dpi(100), -- not really affect anything but it will prevent unnecessary resizing
    forced_height = 0,
}

local player_container   = wibox.widget {
    widget = wibox.container.constraint,
    {
        layout = wibox.layout.stack,
        bg_image,
        {
            widget = wibox.container.background,
            bg = "radial:190,0,0:190,0,400:0.0,#10171397:0.6,#101713",
            {
                layout = wibox.layout.flex.horizontal,
            },
            id = "cover_filter",
        },
        {
            widget = wibox.container.margin,
            margins = {
                left = 10,
                right = 3,
                top = 3,
                bottom = 0,
            },
            {
                layout = wibox.layout.align.vertical,
                {
                    layout = wibox.layout.align.horizontal,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        spacing = 5,
                        prev_player_button,
                        next_player_button,
                    },
                    nil,
                    {
                        widget = wibox.widget.imagebox,
                        forced_height = 15,
                        forced_width = 15,
                        id = "player_app_icon",
                    },
                },
                {
                    layout = wibox.layout.align.horizontal,
                    {
                        layout = wibox.layout.align.horizontal,
                        forced_width = cont_width - 70,
                        nil,
                        {
                            layout = wibox.layout.flex.vertical,
                            {
                                widget = wibox.container.background,
                            },
                            {
                                widget = wibox.widget.textbox,
                                markup = "Title",
                                id = "song_title_text",
                                font = "JetBrainsMono 10",
                                valign = "bottom",
                            },
                            {
                                widget = wibox.widget.textbox,
                                markup = "Artist",
                                id = "song_artist_text",
                                valign = "top",
                            },
                            {
                                widget = wibox.container.background,
                            },
                        },
                        nil,
                    },
                    nil,
                    {
                        widget = wibox.container.margin,
                        margins = {
                            right = 10,
                        },
                        play_button,
                    },
                },
                {
                    layout = wibox.layout.align.horizontal,
                    prev_song_button,
                    {
                        widget = wibox.container.margin,
                        margins = {
                            left = 10,
                            right = 10,
                        },
                        seek_slider,
                    },
                    next_song_button,
                },
            },
        },
    },
    width = cont_width,
    height = cont_height,
}

function player_container:update_ui(player)
    local metadata = mdat_parser(player:print_metadata_prop())
    local art_path = metadata["mpris:artUrl"]

    -- update cover art
    if not art_path then
        bg_image.image = nil
    else
        -- I think the library should also handle file url right?
        if string.sub(art_path, 1, 7) == "file://" then art_path = string.sub(art_path, 8) end

        local stream = awesome.load_image(art_path)
        local surf = gears.surface.load_uncached(stream)

        -- local w = cairo.ImageSurface.get_width(surf)
        local h = cairo.ImageSurface.get_height(surf)

        surf:set_device_offset(0, h / 6)

        bg_image.image = surf
    end

    -- app icon
    if metadata.app then
        local icon_path = icon_lookup(metadata.app)
        self:get_children_by_id("player_app_icon")[1].image = icon_path
    end

    -- update title and artist
    local title = metadata["xesam:title"] or "No Title"
    local artist = metadata["xesam:artist"] or "Unknown Artist"

    -- if #title > 30 then title = title:sub(1, 38) .. "..." end

    self:get_children_by_id("song_title_text")[1].markup = "<b>"
        .. markup(theme.colorscheme[1], gears.string.xml_escape(title))
        .. "</b>\n"

    self:get_children_by_id("song_artist_text")[1].markup =
        markup(theme.colorscheme[5], "by " .. gears.string.xml_escape(artist))

    -- update seek slider
    local length = metadata["mpris:length"]
    if length then
        seek_slider.maximum = tonumber(length)
        seek_slider.visible = true
    else
        seek_slider.visible = false
    end

    -- update prev/next button
    if not player.can_go_next then
        next_song_button.opacity = 0.3
    else
        next_song_button.opacity = 1
    end
    if not player.can_go_previous then
        prev_song_button.opacity = 0.3
    else
        prev_song_button.opacity = 1
    end
end

function player_container:update_player_status(status)
    local plpa_icon = play_button:get_children_by_id("play_pause_icon")[1]
    if status == "PLAYING" then
        plpa_icon.markup = markup(theme.colorscheme[4], "󰏤")
    elseif status == "PAUSED" then
        plpa_icon.markup = markup(theme.colorscheme[4], "󰐊")
    elseif status == "stopped" then
        plpa_icon.markup = markup(theme.colorscheme[4], "󰓛")
    end
end

-- [[
local function init_player_by_name(name)
    local player = playerctl.Player.new_from_name(name)
    player_mgr:manage_player(player)

    -- player_container:update_metadata(player:print_metadata_prop())
    -- player_container:update_player_status(player.playback_status)

    if player.can_seek then
        player_mgr.timer[player.player_name] = gears.timer {
            timeout = 1,
            callback = function ()
                seek_slider:set_value_without_emitting_signal(player.position)
            end,
        }
        if player.playback_status == "PLAYING" then
            player_mgr:start_timer_by_player_name(player.player_name)
            player_mgr:stop_timer_that_not_the_player_name(player.player_name)
        end
        seek_slider:set_value_without_emitting_signal(player.position)
    end

    player_mgr:emit_signal("manager::player_change", player, player_mgr:get_pointer_position(), #player_mgr._players)
end

if #player_mgr.player_names == 0 then player_container.visible = false end

player_mgr:connect_signal("player::playback_status::playing", function (player)
    if player.can_seek then
        player_mgr:start_timer_by_player_name(player.player_name)
        player_mgr:stop_timer_that_not_the_player_name(player.player_name)
    end
end)

player_mgr:connect_signal("player::playback_status::paused", function (player)
    if player.can_seek then player_mgr:stop_timer_by_player_name(player.player_name) end
end)

-- unpredictable, should not use
-- player_mgr:connect_signal("player::playback_status::stopped", function(player, status)
--     player_container:update_player_status("stopped")
-- end)

player_mgr:connect_signal("player::metadata", function (player)
    player_container:update_ui(player)
end)

player_mgr:connect_signal("player::seeked", function (_, pos)
    seek_slider:set_value_without_emitting_signal(pos)
end)

player_mgr:connect_signal("manager::name_appeared", function (_, name)
    player_container.visible = true
    init_player_by_name(name)
end)

player_mgr:connect_signal("manager::name_vanished", function ()
    if #player_mgr.player_names == 0 then
        player_container.visible = false
        return
    end
end)

player_mgr:connect_signal("manager::player_vanished", function (_, player)
    player_mgr:stop_timer_by_player_name(player.player_name)
    player_mgr.timer[player.player_name] = nil
end)

player_mgr:connect_signal("manager::player_change", function (player)
    local total_player = #player_mgr.players

    if total_player == 0 then return end

    local idx = player_mgr:get_pointer_position()
    if idx == 1 then
        prev_player_button.opacity = 0.3
    else
        prev_player_button.opacity = 1
    end

    if idx == total_player then
        next_player_button.opacity = 0.3
    else
        next_player_button.opacity = 1
    end

    player_container:update_ui(player)
    player_container:update_player_status(player.playback_status)
    if player.playback_status == "PLAYING" then player_mgr:start_timer_by_player_name(player.player_name) end
    player_mgr:stop_timer_that_not_the_player_name(player.player_name)
    seek_slider:set_value_without_emitting_signal(player.position)
end)
-- ]]

-- [[ the ui stuff
play_button:connect_signal("button::press", function (_, _, _, btn)
    if btn ~= 1 then return end

    local player = player_mgr:get_active_player()
    player:play_pause()
end)

prev_song_button:connect_signal("button::press", function (_, _, _, btn)
    if btn ~= 1 then return end

    local player = player_mgr:get_active_player()
    player:previous()
end)

next_song_button:connect_signal("button::press", function (_, _, _, btn)
    if btn ~= 1 then return end

    local player = player_mgr:get_active_player()
    player:next()
end)

prev_player_button:connect_signal("button::press", function (_, _, _, btn)
    if btn ~= 1 then return end

    player_mgr:prev_player()
    player_container:update_ui(player_mgr:get_active_player())
end)

next_player_button:connect_signal("button::press", function (_, _, _, btn)
    if btn ~= 1 then return end

    player_mgr:next_player()
    player_container:update_ui(player_mgr:get_active_player())
end)

seek_slider:connect_signal("property::value", function (_, val)
    local player = player_mgr:get_active_player()

    player:set_position(val)
end)
--]]

player_mgr:init_existing_player(init_player_by_name, true)

clickable(play_button)
clickable(next_song_button)
clickable(prev_song_button)
clickable(next_player_button)
clickable(prev_player_button)

return player_container
