local awful = require("awful")
local wibox = require("wibox")
local markup = require("lain.util.markup")
local theme = require("beautiful")

local lgi = require("lgi")
local Gio, GLib = lgi.Gio, lgi.GLib

local function round(n)
    if n - math.floor(n) >= 0.5 then return math.ceil(n) end
    return math.floor(n)
end

local KNOWN_NAME = "dev.cudiph.pulsedbus"
local OBJECT_PATH = "/dev/cudiph/pulsedbus"
local INTERFACE_NAME = "dev.cudiph.pulseaudio"


local function pa_widget_new()
    local widget = wibox.widget {
        widget = wibox.container.background,
        fg = theme.colorscheme[8],
        {

            layout = wibox.layout.fixed.horizontal,
            {
                widget = wibox.widget.textbox,
                markup = "󰕾 -- ",
                id = "sink_textbox",
            },
            {
                widget = wibox.widget.textbox,
                markup = "󰍬 -- ",
                id = "source_textbox",
            },
        },
    }

    local sink_textbox = widget:get_children_by_id("sink_textbox")[1]
    local source_textbox = widget:get_children_by_id("source_textbox")[1]
    local sink_desc_tooltip = awful.tooltip {
        objects = { sink_textbox },
    }
    local source_desc_tooltip = awful.tooltip {
        objects = { source_textbox },
    }

    local function on_getalldefaultinfo_call(connection, res)
        local result = connection:call_finish(res)
        if not result or result:n_children() < 1 then return end
        local _, _, sink_desc, source_desc, sink_vol, source_vol = unpack(result.value)

        sink_desc_tooltip.text = sink_desc
        source_desc_tooltip.text = source_desc

        awful.spawn.easy_async_with_shell("pactl get-sink-mute @DEFAULT_SINK@", function (out)
            local icon = "󰕾 "
            if out:match("yes") then
                icon = "󰝟 "
            end
            sink_textbox.markup = markup.font(theme.wibar_font, icon .. round(sink_vol * 100) .. "% ")
        end)

        awful.spawn.easy_async_with_shell("pactl get-source-mute @DEFAULT_SOURCE@", function (out)
            local icon = "󰍬 "
            if out:match("yes") then
                icon = "󰍭 "
            end
            source_textbox.markup = markup.font(theme.wibar_font, icon .. round(source_vol * 100) .. "% ")
        end)
    end

    local function props_changed_callback(conn)
        conn:call(KNOWN_NAME, OBJECT_PATH, INTERFACE_NAME, "GetAllDefaultInformation", nil,
            GLib.VariantType.new("(ssssdd)"), Gio.DBusCallFlags.NONE, -1, nil,
            on_getalldefaultinfo_call)
    end


    local bus = Gio.bus_get_sync(Gio.BusType.SESSION, nil, nil)
    bus:signal_subscribe(KNOWN_NAME, INTERFACE_NAME, "ServerEvent", OBJECT_PATH, nil, Gio.DBusSignalFlags.NONE,
        props_changed_callback)
    bus:signal_subscribe(KNOWN_NAME, INTERFACE_NAME, "SinkEvent", OBJECT_PATH, nil, Gio.DBusSignalFlags.NONE,
        props_changed_callback)
    bus:signal_subscribe(KNOWN_NAME, INTERFACE_NAME, "SourceEvent", OBJECT_PATH, nil, Gio.DBusSignalFlags.NONE,
        props_changed_callback)

    props_changed_callback(bus) -- for setting initial value

    --[[ using dbusproxy class is not consistent, sometimes the event run sometimes not. ]]
    -- Gio.DBusProxy.new_for_bus(Gio.BusType.SESSION, Gio.DBusProxyFlags.NONE, nil,
    --     KNOWN_NAME,
    --     OBJECT_PATH, INTERFACE_NAME, nil,
    --     function(proxy)
    --         print(proxy)
    --         function proxy:on_g_properties_changed(changed_properties, invalidated_properties, user_data)
    --             if (changed_properties:n_children() == 0) then return end
    --
    --             local type = changed_properties:get_type_string()
    --             print(type)
    --         end
    --
    --         function proxy:on_g_signal(sender_name, signal_name, params, user_data)
    --             print(signal_name)
    --         end
    --     end
    --     , nil)



    return widget
end

return { new = pa_widget_new }
