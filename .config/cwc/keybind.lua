local cful = require("cuteful")
local gears = require("gears")
local cwc = cwc

local enum = cful.enum
local mod = enum.modifier
local button = enum.mouse_btn
local direction = enum.direction

local MODKEY = mod.LOGO
local TERMINAL = "kitty"

-- prevent hotkey conflict on nested session
if cwc.is_nested() then
  MODKEY = mod.ALT
end

local kbd = cwc.kbd

---------------- LAUNCHER -----------------------

kbd.bind({ MODKEY }, "Pause", function()
  cwc.spawn { "hyprlock" }
end)
kbd.bind({ MODKEY }, "backslash", function()
  cwc.spawn_with_shell("swaync-client -t")
end)
