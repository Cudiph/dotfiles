#!/usr/bin/env sh

DEFAULT_SAUCE=$(pactl get-default-source)

pactl set-source-mute "$DEFAULT_SAUCE" toggle

IS_MUTED=$(pactl get-source-mute "$DEFAULT_SAUCE" | gawk '{ print $NF }')

if [ "$IS_MUTED" = 'yes' ]; then
  notify-send -i mic-off STATUS "Muted"
else
  notify-send -i mic-ready STATUS "Unmuted"
fi

