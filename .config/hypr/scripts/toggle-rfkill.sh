#!/usr/bin/env sh

BLOCKED=$(rfkill list | grep 'Soft blocked' | gawk '{print $NF}')
if [ "$BLOCKED" = "no" ]; then
	notify-send -i notification-network-wireless 'Airplane Mode' 'Deactivated'
else
	notify-send -i airplane-mode 'Airplane Mode' 'Activated'
fi
