#!/usr/bin/env sh

pactl set-sink-mute @DEFAULT_SINK@ toggle
IS_MUTED=$(pactl get-sink-mute @DEFAULT_SINK@)

if [ "$IS_MUTED" = 'Mute: yes' ]; then
  notify-send -i notification-audio-volume-muted 'Audio' 'Muted'
else
  notify-send -i notification-audio-volume-high 'Audio' 'Unmuted'
fi
