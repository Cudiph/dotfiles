#!/usr/bin/env sh

STATE_FILE="/run/user/$(id -u)/hyprland-touchpad-disabled"

TOUCHPAD_DEV=$(hyprctl devices | rg "touchpad" | xargs)

if [ -e "$STATE_FILE" ]; then
  hyprctl keyword "device:$TOUCHPAD_DEV:enabled" true
  rm "$STATE_FILE" -f
  notify-send -i input-touchpad-on Touchpad Enabled
else
  hyprctl keyword "device:$TOUCHPAD_DEV:enabled" false
  touch "$STATE_FILE"
  notify-send -i input-touchpad-off Touchpad Disabled
fi


