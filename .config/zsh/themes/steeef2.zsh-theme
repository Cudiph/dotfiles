# prompt style and colors based on Steve Losh's Prose theme:
# https://github.com/sjl/oh-my-zsh/blob/master/themes/prose.zsh-theme
#
# vcs_info modifications from Bart Trojanowski's zsh prompt:
# http://www.jukie.net/bart/blog/pimping-out-zsh-prompt
#
# git untracked files modification from Brian Carper:
# https://briancarper.net/blog/570/git-info-in-your-zsh-prompt

export VIRTUAL_ENV_DISABLE_PROMPT=1

function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo '('%F{blue}`basename $VIRTUAL_ENV`%f') '
}
PR_GIT_UPDATE=1

setopt prompt_subst

autoload -U add-zsh-hook
autoload -Uz vcs_info

PR_RST="%f"

#use extended color palette if available
if [[ $COLORTERM == "truecolor" ]]; then
    turquoise=$'%{\e[38;2;161;224;212m%}'
    orange=$'%{\e[38;2;224;201;162m%}'
    purple=$'%{\e[38;2;200;166;225m%}'
    red=$'%{\e[38;2;240;142;151m%}'
    hotpink=$'%{\e[38;2;240;142;151m%}'
    green=$'%{\e[38;2;167;225;164m%}'
    limegreen=$'%{\e[38;2;177;235;174m%}'
    yellow=$'%{\e[38;2;213;224;163m%}'
    PR_RST=$'%{\e[m%}'
elif [[ $terminfo[colors] -ge 256 ]]; then
    turquoise="%F{81}"
    orange="%F{166}"
    purple="%F{135}"
    hotpink="%F{161}"
    limegreen="%F{118}"
    yellow="%F{226}"
else
    turquoise="%F{cyan}"
    orange="%F{yellow}"
    purple="%F{magenta}"
    hotpink="%F{red}"
    limegreen="%F{green}"
    yellow="%F{yellow}"
fi

# enable VCS systems you use
zstyle ':vcs_info:*' enable git svn

# check-for-changes can be really slow.
# you should disable it, if you work with large repositories
zstyle ':vcs_info:*:prompt:*' check-for-changes true

# set formats
# %b - branchname
# %u - unstagedstr (see below)
# %c - stagedstr (see below)
# %a - action (e.g. rebase-i)
# %R - repository path
# %S - path in the repository
FMT_BRANCH="(%{$turquoise%}%b%u%c${PR_RST})"
FMT_ACTION="(%{$limegreen%}%a${PR_RST})"
FMT_UNSTAGED="%{$orange%}●"
FMT_STAGED="%{$limegreen%}●"

zstyle ':vcs_info:*:prompt:*' unstagedstr   "${FMT_UNSTAGED}"
zstyle ':vcs_info:*:prompt:*' stagedstr     "${FMT_STAGED}"
zstyle ':vcs_info:*:prompt:*' actionformats "${FMT_BRANCH}${FMT_ACTION}"
zstyle ':vcs_info:*:prompt:*' formats       "${FMT_BRANCH}"
zstyle ':vcs_info:*:prompt:*' nvcsformats   ""


TIME_BEFORE=''
TIME_TAKEN=''

function steeef_preexec {
    case "$2" in
        *git*)
            PR_GIT_UPDATE=1
            ;;
        *hub*)
            PR_GIT_UPDATE=1
            ;;
        *svn*)
            PR_GIT_UPDATE=1
            ;;
    esac
    TIME_BEFORE=$EPOCHREALTIME
}
add-zsh-hook preexec steeef_preexec

function steeef_chpwd {
    PR_GIT_UPDATE=1
}
add-zsh-hook chpwd steeef_chpwd

function steeef_precmd {
    if [[ -n "$PR_GIT_UPDATE" ]] ; then
        # check for untracked files or updated submodules, since vcs_info doesn't
        if git ls-files --other --exclude-standard 2> /dev/null | grep -q "."; then
            PR_GIT_UPDATE=1
            FMT_BRANCH="(%{$turquoise%}%b%u%c%{$hotpink%}●${PR_RST})"
        else
            FMT_BRANCH="(%{$turquoise%}%b%u%c${PR_RST})"
        fi
        zstyle ':vcs_info:*:prompt:*' formats "${FMT_BRANCH} "

        vcs_info 'prompt'
        PR_GIT_UPDATE=
    fi

    if [[ "$TIME_BEFORE" -ne '' ]]; then
        TIME_TAKEN=$(( $EPOCHREALTIME - $TIME_BEFORE ))
        if [[ "$TIME_TAKEN[0,4]" -eq "0.00" ]]; then
            TIME_TAKEN="0s"
        else
            TIME_TAKEN="$TIME_TAKEN[0,4]s"
        fi
    fi
}
add-zsh-hook precmd steeef_precmd

PROMPT=$'
%B%{$purple%}%n${PR_RST}%b at %B%{$orange%}%m${PR_RST}%b in %B%{$limegreen%}%~${PR_RST}%b $vcs_info_msg_0_$(virtualenv_info)%B%{$hotpink%}[%*]${PR_RST}%b %B%{$yellow%}$TIME_TAKEN${PR_RST}%b
%B%(?:%{$green%}:%{$red%}[%?] )%(!.#.$)%b ${PR_RST}%b'
