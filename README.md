<div align="center">
  <h1>Preview</h1>
  <img src="./gallery/preview.png" alt="preview image" />
</div>

## X11 setup

- WM: AwesomeWM (git)
- Bar: wibar
- Notification: naughty
- Compositor: jonaburg-picom

## Wayland setup

- Compositor: Hyprland
- Bar: Waybar
- Notification: dunst

## Misc

- Terminal: kitty
- Shell: zsh
- Icons: Papirus
- Font: JetBrainsMono + Hack + Roboto (Nerd Font)

## Environment variable

- `OPENWEATHER_APIKEY`
- `OPENWEATHER_CITY_ID`
- `QT_QPA_PLATFORMTHEME=qt5ct`

## Installing in new system/user

Installation can be done by running the install script:

```sh
curl -fsSL https://gitlab.com/Cudiph/dotfiles/-/raw/main/install.sh | bash -s all
```

The script will create symlink for the configuration so it doesn't pollute your
root home directory. Or if you prefer the "hard/native" way, you can visit
[atlassian tutorial](https://www.atlassian.com/git/tutorials/dotfiles) for more
information. If the configuration is not applied or the script failed, you may
tweaks the configuration manually. (Installing in a newly created user is
reccommended)

### Notes

- Updating submodule to latest version:

```sh
dotfig submodule update --remote --recursive
```

- if zsh took longer to start, probably .zshrc sourcing .zshrcpp twice

- if nvim asking for example configuration, just press enter (doesn't matter
what to choose since the example configuration will be deleted anyway)
