#!/usr/bin/env bash

# check git
git --help >/dev/null
if [ $? -eq 127 ]; then
	echo "git was not found, please install it first"
	exit 127
fi

# env var setup
CFG_ROOT=${XDG_CONFIG_HOME:-$HOME/.config}
CUSTOMCONFIG_ROOT=$CFG_ROOT/cudiphfiles
REPO_ROOT=${XDG_DATA_HOME:-$HOME/.local/share}/cudiphfiles
ZDOTDIR="$CFG_ROOT/zsh"

mkdir -p "$CUSTOMCONFIG_ROOT"

clone_repo() {
	if [ ! -d "$REPO_ROOT" ]; then
		echo "$CUSTOMCONFIG_ROOT"
		git clone --bare https://gitlab.com/Cudiph/dotfiles "$REPO_ROOT" --recurse-submodule --depth 1
		git --git-dir="$REPO_ROOT" --work-tree="$CUSTOMCONFIG_ROOT" checkout main
		git --git-dir="$REPO_ROOT" --work-tree="$CUSTOMCONFIG_ROOT" -C "$CUSTOMCONFIG_ROOT" submodule update --recursive --init
	else
		echo "repository already exist, quiting..."
		exit
	fi
}

install_packages() {
	which yay &>/dev/null
	if [ $? -eq 1 ]; then
		echo "yay wasn't found, installing..."
		sudo pacman --needed -S base-devel
		git clone https://aur.archlinux.org/yay-bin.git
		cd yay-bin || exit 2
		makepkg -si
		cd .. || exit 2
		rm -rf yay-bin
	fi
	echo "Installing required packages..."
	REQUIRED_PACKAGES=(
		"sddm" "awesome-git" "zsh" "neovim" "unclutter" "which" "luarocks" "make" "pkgconf" "xorg-xinput" # window manager
		"pipewire" "wireplumber" "pipewire-audio" "pipewire-alsa" "pipewire-pulse" "pipewire-jack"        # audio
		"shellcheck" "gcc"                                                                                # dev stuff
	)
	yay --needed -S "${REQUIRED_PACKAGES[@]}"

	read -erp "would you like to install optional packages?[y/N] " opt_bool
	if [ "$opt_bool" != "y" ]; then return; fi

	OPTIONAL_PACKAGES=(
		# desktop extensions
		"flameshot" "brightnessctl" "redshift" "fcitx5" "lxappearance" "xorg" "xcolor"
		"playerctl" "bluez" "bluez-utils" "blueman" "copyq" "network-manager-applet" "qt5ct" "onboard"
		"flatpak" "libnotify" "mate-polkit"
		# dev
		"gdb" "nodejs-lts-hydrogen" "npm" "gef" "docker" "docker-buildx" "docker-compose" "lib32-gcc-libs"
		# utilities
		"ripgrep" "dust" "fd" "usbutils" "htop" "mtr" "strace" "ltrace" "wget" "gnu-netcat"
		"man-db" "man-pages" "less" "android-tools" "gvfs-mtp" "bat" "fzf" "yt-dlp" "p7zip"
		"nethogs" "lsof" "eza" "tree" "dash" "dkms"
		# fonts
		"noto-fonts" "noto-fonts-cjk" "noto-fonts-emoji" "noto-fonts-extra"
		"ttf-jetbrains-mono-nerd" "ttf-hack-nerd" "ttf-roboto-mono-nerd" "ttf-terminus-nerd"
		"ttf-inconsolata-nerd" "ttf-terminus-nerd" "otf-droid-nerd" "otf-firamono-nerd"
		# gui apps
		"mpv" "firefox" "alacritty" "kitty" "pcmanfm-qt" "engrampa"
		"notepadqq" "telegram-desktop" "libreoffice-still" "thunderbird"
		# multimedia
		"evince" "gimp" "inkscape" "quodlibet" "feh" "pavucontrol" "zathura" "zathura-pdf-poppler"
		"zathura-djvu" "zathura-cb" "ffmpegthumbnailer" "gst-plugins-good" "faad2" "gst-libav"
	)
	touch "${CFG_ROOT}/kitty/personal.conf"

	FLATPAK_PKGS=(
		"io.github.spacingbat3.webcord" "com.valvesoftware.Steam" "org.winehq.Wine"
	)

	echo ""
	echo "Installing optional packages..."
	sudo pacman --needed -S "${OPTIONAL_PACKAGES[@]}"
	flatpak install "${FLATPAK_PKGS[@]}"
}

setup_zsh() {
	echo "Setting up zsh..."
	echo "source $ZDOTDIR/.zshrcpp" | tee -a "$CUSTOMCONFIG_ROOT/.config/zsh/.zshrc"
	echo ""
	echo "Adding ZDOTDIR env var to $HOME/.zshenv"
	echo "ZDOTDIR=\${XDG_CONFIG_HOME:-\$HOME/.config}/zsh" | tee -a "$HOME/.zshenv"
	echo ""
	echo "changing default shell to zsh"
	chsh -s "$(which zsh)"
}

create_symlink() {
	echo "Creating configuration symbolic link"
	cd "$CUSTOMCONFIG_ROOT/.config" || exit 63
	stow -vt "$CFG_ROOT" .
}

destroy_symlink() {
	echo "Deleting configuration symbolic link"
	cd "$CUSTOMCONFIG_ROOT/.config" || exit 63
	stow -vt "$CFG_ROOT" --delete .
}

run_all() {
	echo "Starting script in 3 seconds, press ctrl+c to cancel..."
	sleep 3
	clone_repo
	install_packages
	setup_zsh
	create_symlink
}

install_aurpkgs() {
	AUR_PACKAGES=(
		"catppuccin-gtk-theme-mocha" "reversal-icon-theme-git" "adwaita-qt5-git" "mcmojave-cursors"
		"genymotion" "qdirstat" "pipes.sh" "tty-clock" "ani-cli" "sddm-sugar-dark"
	)

	yay --needed -S "${AUR_PACKAGES[@]}"
}

install_ctfpkgs() {
	CTF_PKGS=(
		# reverse engineering
		"jadx" "ida-free" "genymotion" "android-apktool-bin" "python-z3-solver" "ghidra"
		"bless" "imhex-bin" "pycdc-git"
		# binary exploitation
		"patchelf" "pwninit-bin" "python-pwntools"
		# web
		"zaproxy" "wireshark-qt" "wireshark-cli"
		# forensic
		"stegsolve"
		# crypto
		"sagemath" "python-pycryptodome"
	)
	yay --needed -S "${CTF_PKGS[@]}"
}
setup_virtualization() {
	VIRT_PKGS=(
		"aarch64-linux-gnu-gcc" "aarch64-linux-gnu-gdb"
		"qemu-full" "virt-manager" "virt-viewer" "dnsmasq" "swtpm" "libguestfs"
	)

	yay --needed -S "${VIRT_PKGS[@]}"
	sudo usermod -aG libvirt "$USER"
	sudo systemctl enable --now libvirtd.socket
}

setup_wayland() {
	WAYPKGS=(
		"waybar" "swayidle" "xdg-desktop-portal-wlr" "rofi-lbonn-wayland"
		"slurp" "grim" "dunst" "swww" "eww"
	)

	yay --needed -S "${WAYPKGS[@]}"
}

setup_misc() {
	# sddm themes
	cat <<EOF | sudo tee /etc/sddm.conf >/dev/null
[Theme]
Current=sugar-dark
EOF
	SDDM_THEME_PATH=/usr/share/sddm/themes/sugar-dark/theme.conf
	sudo sed -ie "s/navajowhite/#c8a6e1/g; s/HeaderText=Welcome!/HeaderText=Welcome to SlopLive!/g; s/=\"white\"/=\"#b4b8e6\"/g" $SDDM_THEME_PATH

	# /bin/sh as dash
	sudo ln -sf dash /bin/sh
}

show_help() {
	cat <<EOF
EXAMPLE:
  ./install.sh symlink
  ./install.sh virt

SUBCOMMAND:
  all           run every subcommands (clean environment reccomended)
  pkgs          install required+optional packages 
  zsh           setup zsh environment
  symlink       create symlink for \$XDG_CONFIG_HOME or ~/.config
  undo-symlink  delete symlinks created by symlink command
  clone         setup git repository from upstream

EXTRA:
  aurpkgs       install extra packages from AUR
  ctfpkgs       tools for ctf challenge
  virt          packages for working in non x86 stuff
  wayland       wayland packages
  misc          misc. script
EOF
	exit 1
}

case "$1" in
"all") run_all ;;
"pkgs") install_packages ;;
"zsh") setup_zsh ;;
"symlink") create_symlink ;;
"undo-symlink") destroy_symlink ;;
"clone") clone_repo ;;

# extra
"aurpkgs") install_aurpkgs ;;
"ctfpkgs") install_ctfpkgs ;;
"wayland") setup_wayland ;;
"virt") setup_virtualization ;;
"misc") setup_misc ;;
*) show_help ;;
esac
